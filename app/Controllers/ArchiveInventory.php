<?php

namespace App\Controllers;

use App\Automotive\Helpers\Templating;
use Sober\Controller\Controller;

class ArchiveInventory extends Controller
{
    /**
     * @return array|string|void
     */
    public function pagination() {
        global $wp_query;

        return Templating::getPaginationHtml($wp_query);
    }

    public function orderConfig() {
        return ['date:ASC' => 'Date: newest first', 'date:DESC' => 'Date: oldest first', 'price:ASC' => 'Price: lower first', 'price:DESC' => 'Price: highest first', 'odometer:ASC' => 'Mileage: lower first', 'odometer:DESC' => 'Mileage: highest first'];
    }

    public static function isOrderSelected( string $order ) {
        $queryCookie = isset($_COOKIE['queryConfig']) ? json_decode( str_replace('\\', '', $_COOKIE['queryConfig']), true ) : [];

        if(empty($queryCookie)) return '';

        $orderArr = explode( ':', $order);
        if($queryCookie['orderby'] === $orderArr[0] && $queryCookie['order'] === $orderArr[1]) {
            return ' selected';
        }

        return '';
    }

    public static function outputQueryConfig(  ) {
        return isset($_COOKIE['queryConfig']) ? $_COOKIE['queryConfig'] : '';
    }
}
