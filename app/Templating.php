<?php


namespace App;


use App\Automotive\Features\ACFOptimization;

class Templating {
    public static function getField( string $name, int $id = 0 ) {
        if($id === 0){
            return ACFOptimization::get($name);
        } else {
            return ACFOptimization::get($name, $id);
        }

    }
}
