<?php


namespace App\Automotive;


abstract class AbstractHandler
{
    public static function removeActions(array $actionsArr){
        if(!empty($actionsArr)){
            foreach ($actionsArr as $action){
                switch (true){
                    case empty($action['priority']):
                        remove_action($action['actionName'], $action['function']);
                        break;

                    default:
                        remove_action($action['actionName'], $action['function'], $action['priority']);
                }
            }
        }
    }

    public static function addActions(array $actionsArr){
        if(!empty($actionsArr)){
            foreach ($actionsArr as $action){
                switch (true){
                    case empty($action['priority']):
                        add_action($action['actionName'], $action['function']);
                        break;

                    case empty($action['params']):
                        add_action($action['actionName'], $action['function'], $action['priority']);
                        break;

                    default:
                        add_action($action['actionName'], $action['function'], $action['priority'], $action['params']);
                }
            }
        }
    }
}
