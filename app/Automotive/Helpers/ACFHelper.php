<?php


namespace App\Automotive\Helpers;


class ACFHelper {
    public static function updateSingleFieldCache( $value, $post_id, array $field ) {
        $option = [];
        if($option = get_option('automotive_' . $field['name'])){
            $option = maybe_unserialize($option);
        }

        if(!array_search( $value, $option)) $option[] = $value;

        update_option('automotive_' . $field['name'], $option, false);

        return $value;
    }

    public static function loadCachedFieldValues( array $field ) {
        $option = [];
        if($option = get_option('automotive_' . $field['name'])){
            switch ($field['type']){
                case 'select':
                    $option = array_combine( $option, $option);
                    break;
            }
        }

        $field['choices'] = array_merge( $field['choices'], $option);
        return $field;
    }

    public static function loadCachedFieldValuesByName( string $name, string $type = 'string' ) {
        $option = [];
        if($option = get_option('automotive_' . $name)){
            switch ($type){
                case 'select':
                    $option = array_combine( $option, $option);
                    break;
            }
        }

        return $option;
    }

    public static function setMaxPriceOption($value, $post_id, array $field) {
        $option = get_option('maxPrice');

        if($option && (int) $value < (int) $option){
            update_option( 'maxPrice', (int)$value, false);
        } else {
            update_option('maxPrice', (int)$value, false);
        }

        return $value;
    }
}
