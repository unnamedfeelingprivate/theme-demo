<?php


namespace App\Automotive\Helpers;


use WP_Query;

class Templating {
    public static function outputSocials(  ) {
        $html = '';

        if(get_field('general_social_links', 'option')){
            $html = '<ul class="s">%s</ul>';

            $links = '';
            foreach (get_field('general_social_links', 'option') as $social){
                if ($social['link']){
                    $links .= sprintf(
                        '<li><a href="%s" target="_blank">%s</a></li>',
                        $social['link'],
                        !empty($social['icon']) ? "<i class=\"{$social['icon']}\"></i>" : ''
                    );
                }
            }

            $html = sprintf($html, $links);
        }

        return $html;
    }

    public static function getPaginationHtml(WP_Query $query){
        $qMaxNumCount = ceil(count($query->posts) / 6);
        $maxNumPages = $qMaxNumCount > $query->max_num_pages ? $qMaxNumCount : $query->max_num_pages;
//        $result = $maxNumPages.PHP_EOL.json_encode($query->query_vars, JSON_PRETTY_PRINT).PHP_EOL;
        $result = '';
        if($maxNumPages > 1) {
            $current = max( 1, $query->get( 'paged' ) );
            $links   = paginate_links( array(
                'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
                'total'        => $maxNumPages,
                'current'      => $current,
                'format'       => '?paged=%#%',
                'show_all'     => false,
                'type'         => 'array',
                'end_size'     => 2,
                'mid_size'     => 1,
                'prev_next'    => false,
                'add_args'     => false,
                'add_fragment' => '',
            ) );

            $prev_link = sprintf(
                '<a class="page-numbers pagination-link" href="%s"%s><i class="icon-chevron-thin-left"></i></a><div class="pages">',
                $current > 1 ? '/inventory/page/' . ( $current - 1 ) . '/' : '#',
                $current > 1 ? 'data-page="' . ( $current - 1 ) . '"' : ''
            );
            $next_link = sprintf(
                '</div><a class="page-numbers pagination-link" href="%s"%s><i class="icon-chevron-thin-right"></i></a>',
                $current < $maxNumPages ? '/inventory/page/' . ( $current + 1 ) . '/' : '#',
                $current < $maxNumPages ? 'data-page="' . ( $current + 1 ) . '"' : ''
            );

            array_unshift( $links, $prev_link );
            $links[] = $next_link;

            $result .= implode( '', $links );
        }

        return $result;
    }
}
