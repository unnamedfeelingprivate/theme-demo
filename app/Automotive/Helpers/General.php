<?php


namespace App\Automotive\Helpers;

use WP_Post;

class General {
    public static function getOptionsGroup( string $name ) {
        return get_field( $name, 'option' );
    }
    public static function returnGoogleMapsApiKey() {
        $group = General::getOptionsGroup( 'general' );
        return $group['google_maps_api_key'] ? : '';
    }

    public static function setAcfGoogleApiKey( array $api ){
        $api['key'] = General::returnGoogleMapsApiKey();

        return $api;
    }

    public static function mainScriptLocalization(  ) {
        $values = [
            'tpld' => get_template_directory_uri(),
            'googleApiKey' => General::returnGoogleMapsApiKey(),
        ];

        return $values;
    }

    /**
     * This is a modification of image_downsize() function in wp-includes/media.php
     * we will remove all the width and height references, therefore the img tag
     * will not add width and height attributes to the image sent to the editor.
     *
     * @param bool false No height and width references.
     * @param int $id Attachment ID for image.
     * @param array|string $size Optional, default is 'medium'. Size of image, either array or string.
     * @return bool|array False on failure, array on success.
     */
    public static function imageHWFilter( bool $value, $id, $size ) {
        if ( !wp_attachment_is_image($id) )
            return false;

        $img_url = wp_get_attachment_url($id);
        $is_intermediate = false;
        $img_url_basename = wp_basename($img_url);

        // try for a new style intermediate size
        if ( $intermediate = image_get_intermediate_size($id, $size) ) {
            $img_url = str_replace($img_url_basename, $intermediate['file'], $img_url);
            $is_intermediate = true;
        }
        elseif ( $size === 'thumbnail' ) {
            // Fall back to the old thumbnail
            if ( ($thumb_file = wp_get_attachment_thumb_file($id)) && $info = getimagesize($thumb_file) ) {
                $img_url = str_replace($img_url_basename, wp_basename($thumb_file), $img_url);
                $is_intermediate = true;
            }
        }

        // We have the actual image size, but might need to further constrain it if content_width is narrower
        if ( $img_url && !in_array($size, ['full', 'showroom-main'])) {
            return array( $img_url, 0, 0, $is_intermediate );
        }

        return false;
    }

    public static function remove_width_attribute( $html ) {
        $html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
        return $html;
    }

    public static function addIconToMenuItems( $items, $args ){
        foreach ($items as $item){
            /**
             * @var WP_Post $item
             */
            $icon = get_field('icon', $item);
            $iconpos = get_field('icon_position', $item) ?: 'After';
            $iconHtml = $icon ? sprintf('<i class="%s"></i>', $icon) : '';

            $item->title = sprintf(
                '%s%s%s',
                $iconpos === 'Before' ? $iconHtml : '',
                $item->title,
                $iconpos === 'After' ? $iconHtml : ''
            );

            $item->classes[] = !$icon ? :'with-icon';
        }

        return $items;
    }

    public static function wrapGutenbergBlocks($block_content, $block){
        if(!empty($block['blockName'])){
            if ( isset( $block['attrs']['align'] ) && in_array( $block['attrs']['align'], array( 'wide', 'full' ) ) ) {
                $block_content = sprintf(
                    '<div class="%1$s">%2$s</div>',
                    'align-wrap align-wrap-' . esc_attr( $block['attrs']['align'] ),
                    $block_content
                );
            }

            $trimmedBlockName = substr( $block['blockName'], strpos($block['blockName'], '/')+1, strlen($block['blockName']) );

            switch (true){
                case ($block['blockName'] === 'wp-bootstrap-blocks/container'):
                    $containerWrapper = '<section class="%s" id="%s">%s</section>';

                    return sprintf(
                        $containerWrapper,
                        'wp-block-' . $trimmedBlockName,
                        $block['blockName'].'-'.sha1(json_encode($block)),
                        $block_content
                    );

//                case ($block['blockName'] === 'wp-bootstrap-blocks/row'):
//                    $containerWrapper = '<section class="%s" id="%s">%s</section>';
//
//                    return sprintf(
//                        $containerWrapper,
//                        'wp-block-' . $trimmedBlockName,
//                        $block['blockName'].'-'.sha1(json_encode($block)),
//                        $block['attrs']['fullwidth'] ? '-fluid' : '',
//                        $block_content.json_encode($block)
//                    );

                case ($block['blockName'] === 'core/columns'):
                    $containerWrapper = '<section class="%s" id="%s"><div class="container%s">%s</div></section>';

                    return sprintf(
                        $containerWrapper,
                        'wp-block-' . $trimmedBlockName,
                        $block['blockName'].'-'.sha1(json_encode($block)),
                        $block['attrs']['fullwidth'] ? '-fluid' : '',
                        $block_content.json_encode($block)
                    );

                default:
                    return $block_content;
            }

//            $containerWrapper = '<section class="%s" id="%s"><div class="container%s"><div class="row">%s</div></div></section>';
//
//            return sprintf(
//                $containerWrapper,
//                'wp-block-' . $trimmedBlockName,
//                $block['blockName'].'-'.sha1(json_encode($block)),
//                $block['attrs']['fullwidth'] ? '-fluid' : '',
//                $block_content.json_encode($block)
//            );
        }

        return $block_content;
    }
}
