<?php


namespace App\Automotive\Features;

use App\Automotive\Features\Filters\BackendFilters;
use App\Automotive\Features\Filters\FrontentFilterForm;
use App\Automotive\Features\Filters\Widget;
use Formr;
use WP_Query;
use WP_REST_Request;

class Filters
{
    public function __construct()
    {
    }

    public function run()
    {
        $this->initFilters();
        $this->initActions();
        $this->handleGetParams();
        add_action('pre_get_posts', [$this, 'limitPerpage'], 100, 1);
        add_action('rest_api_init', function () {
            $this->initAPI();
        });
    }

    private function initAPI()
    {
            register_rest_route('automotive/v1', '/fetchInventory', array(
                'methods' => 'POST',
//                'callback' => ['App\Automotive\Features\Filters\BackendFilters', 'fetchInventory'],
                'callback' => function (WP_REST_Request $request) {
                    $filters = new BackendFilters();
                    $result = $filters->fetchInventory($request);

                    if($result['error']) return wp_send_json_error($result);

                    return wp_send_json_success($result);
                },
            ));
    }

    private function initFilters()
    {
    }

    private function initActions()
    {
        add_action('widgets_init', function () {
            register_widget('App\Automotive\Features\Filters\Widget');
        });
    }

    public static function outputFilters()
    {
        $frontendFilter = new FrontentFilterForm();

        return $frontendFilter->renderForm();
    }

    public static function limitPerpage(WP_Query $query) {
        if (!is_admin() && $query->is_main_query() && ($query->is_post_type_archive('inventory') || $query->is_tax('condition'))) {
            $query ->set('posts_per_page', 6);

            self::handleGlobalQueryConfigFromCookie( $query);
        }
    }

    private function handleGetParams()
    {
        if (empty($_GET)) {
            return false;
        }

        add_action('pre_get_posts', [BackendFilters::class, 'overrideQuery']);
    }

    private static function handleGlobalQueryConfigFromCookie(WP_Query $query) {
        $queryConfig = isset($_COOKIE['queryConfig']) ? json_decode( str_replace('\\', '', $_COOKIE['queryConfig']), true ) : [];

        if(!empty($queryConfig)) {
            if(!empty($queryConfig['order'])) $query->set('order', $queryConfig['order']);
            if(!empty($queryConfig['order_by'])){
                switch (true){
                    case ($queryConfig['order_by'] === 'price'):
                    case ($queryConfig['order_by'] === 'odometer'):
                        $currMetaQuery = $query->get('meta_query') ? $query->get('meta_query') : [];
                        $currMetaQuery[$queryConfig['order_by']] = [
                            'key' => $queryConfig['order_by'],
                            'compare' => 'EXISTS'
                        ];
                        if(!isset($currMetaQuery['relation'])) $currMetaQuery['relation'] = 'AND';
                        $query->set('meta_query', $currMetaQuery);
                        $query->set('orderby', $queryConfig['order_by']);
                        break;
                    default:
                        $query->set('orderby', 'date');
                }
            }
        }
    }
}
