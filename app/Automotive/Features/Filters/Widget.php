<?php


namespace App\Automotive\Features\Filters;


use App\Automotive\Features\Filters;
use App\Automotive\Helpers\ACFHelper;
use Formr;
use WP_Widget;

class Widget extends WP_Widget {
    function __construct() {
        parent::__construct(
            'automotive_filter_widget',
            __('Filter Widget', 'automotive'),
            array( 'description' => __( 'Simple filter widget', 'automotive' ), )
        );
    }

    public function widget( $args, $instance ) {
        echo Filters::outputFilters();
    }

    // Widget Backend
    public function form($instance) {
        $fieldsConf = $this->getFormConfig();

        $form = new Formr();
        foreach ($fieldsConf as $key => $value){
            if(isset($instance[$key])) $value['value'] = $instance[$key];

            switch ($fieldsConf[$key]['type']){
                case 'select':
                    printf(
                        '<p>%s</p>',
                        $form->input_select($value)
                    );
                    break;

                default:
                    printf(
                        '<p>%s</p>',
                        $form->input_text($value)
                    );
            }

        }
    }

    private function getFormConfig(){
        $fieldsConf = [
            'title' => [
                'name' => $this->get_field_name('title'),
                'label' => __('Title', 'automotive'),
                'id' => $this->get_field_id('title'),
                'type' => 'text'
            ],
//            'fields' => [
//                'type' => 'select',
//                'name' => $this->get_field_name('fields'),
//                'label' => __('Fields', 'automotive'),
//                'value' => '',
//                'string' => '',
//                'inline' => '',
//                'selected' => '',
//                'options' => ACFHelper::loadCachedFieldValuesByName('makes'),
//                'id' => $this->get_field_id('fields'),
//                'multiple' => 'multiple',
//            ],
        ];

        return $fieldsConf;
    }

    // Updating widget replacing old instances with new
    public function update($new_instance, $old_instance) {
        $instance = [];
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        return $instance;
    }
}
