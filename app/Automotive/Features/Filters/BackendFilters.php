<?php

// TODO: refactor main method
// TODO: output error data


namespace App\Automotive\Features\Filters;

use App\Automotive\Helpers\Templating;
use WP_Post;
use WP_Query;
use WP_REST_Request;
use function App\template;

class BackendFilters
{
    public static function overrideQuery(WP_Query $query)
    {
        if (!is_admin() && $query->is_main_query() && $query->query['post_type'] === 'inventory') {
            $metaQuery = ['relation' => 'AND'];

            foreach ($_GET as $key => $val) {
                $metaQuery[] = [
                    'key' => $key,
                    'value' => $val
                ];
            }

            if (!empty($metaQuery)) {
                $query ->set('meta_query', $metaQuery);
            }
        }

        return $query;
    }

    public function fetchInventory(WP_REST_Request $request)
    {
        $params = $request->get_params();

        $queryArgs = [
            'post_type' => 'inventory',
            'posts_per_page' => -1,
            'order' => isset($params['order']) ? $params['order'] : 'ASC',
            'orderby' => isset($params['orderby']) ? $params['orderby'] : 'ID',
            'meta_query' => [],
        ];

        $result = [
            'error' => true,
            'params' => $params,
            'queryArgs' => $queryArgs
        ];

        //                $metaArr = ['makes', 'bodies', 'models', 'price'];
        $metaArr = ['makes', 'bodies', 'models'];

        if (empty($params)) {
            $result['message'] = __('Something wrong with your request!', 'automotive');
        }

        if(!isset($params['filterNonce']) || !wp_verify_nonce($params['filterNonce'], 'automotive_filterNonce')) {
            $result['message'] = __( 'Go and fuck yourself!', 'automotive' );
            return $result;
        }

//        if(isset($params['hash'])){
//            $transient = !empty(array_diff($params, $metaArr)) ? get_transient($params['hash']) : null;
            $transient = isset($params['hash']) ? get_transient($params['hash']) : null;

            $result['transient'] = $transient;

            if ($transient){
                $queryArgs['post__in'] = maybe_unserialize($transient);

                $result['queryArgs'] = $queryArgs;

                $queryRes = isset($params['hash']) ?
                    $this->queryPosts($queryArgs, $params['hash']) : $this->queryPosts($queryArgs);

                if(!empty($queryRes['html'])){
                    $result['html'] = $queryRes['html'];
                    $result['pagination'] = $queryRes['pagination'];
                    $result['error'] = false;
                } else {
                    $result['message'] = __('Nothing found!', 'automotive');
                }
            } else {
                foreach ( $metaArr as $meta ) {
                    switch ($meta){
                        case 'price':
                            if(isset($params['min_price']) && isset($params['max_price'])){
                                $queryArgs['meta_query'][$meta] = [
                                    'key' => $meta,
                                    'value' => [(int) $params['min_price'], (int) $params['max_price']],
                                    'type' => 'numeric',
                                    'compare' => 'BETWEEN',
                                ];
                            }
                            break;

                        default:
                            if(isset($params[$meta])){
                                $queryArgs['meta_query'][$meta] = [
                                    'key' => $meta,
                                    'value' => $params[$meta]
                                ];
                            }
                    }

                }

                if(!empty($queryArgs['meta_query'])) {
                    $queryArgs['meta_query']['relation'] = 'AND';
                }

                $result['queryArgs'] = $queryArgs;

                $queryRes = isset($params['hash']) ?
                    $this->queryPosts($queryArgs, $params['hash']) : $this->queryPosts($queryArgs);

                if(!empty($queryRes['html'])){
                    $result['html'] = $queryRes['html'];
                    $result['pagination'] = $queryRes['pagination'];
                    $result['error'] = false;
                } else {
                    $result['message'] = __('Nothing found!', 'automotive');
                }
            }
//        }

        return $result;
    }

    private function queryPosts(array $args = [], string $hash = '') {
        $result = [
            'html'      => null,
            'transient' => [],
        ];

        if ( empty( $args ) ) return $result;

        $query = new WP_Query( $args );

        $result['query'] = $query;

        if ( $query->have_posts() ) {
            $i = 0;
            while($query->have_posts()){
                global $post;
                $query->the_post();
                /**
                 * @var WP_Post $post
                 */

                $result['transient'][] = $post->ID;
            }
            wp_reset_postdata();

            ob_start();
            while ( $i < 6 ):
                global $post;
                $query->the_post();
                /**
                 * @var WP_Post $post
                 */

                echo template( 'post-types.inventory.loop.loop-inventory', [ 'post' => $post ] );
                $i++;
            endwhile;

            wp_reset_postdata();

            if(!empty($hash)){
                set_transient($hash, serialize($result['transient']), HOUR_IN_SECONDS);
            }

            $result['html'] = ob_get_clean();
        }

        $result['pagination'] = Templating::getPaginationHtml($query);

        return $result;
    }
}
