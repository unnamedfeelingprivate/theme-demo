<?php


namespace App\Automotive\Features\Filters;


use App\Automotive\Helpers\ACFHelper;
use Formr;
use function App\template;

class FrontentFilterForm {
    private $wp_query;

    private $form;

    public function __construct() {
        global $wp_query;

        $this->wp_query = $wp_query;
        $this->form = new Formr('bootstrap');
    }

    public function renderForm() {
        return template('ajax.filter.filterWidget', [
            'form' => $this->getForm(),
            'wp_query' => $this->wp_query,
            'getData' => $_GET
        ]);
    }

    private function getForm(){
        $html = $this->form->form_open_multipart(
            'FilterForm',
            'FilterForm',
            home_url('/wp-json/automotive/v1/fetchInventory'),
            'POST'
        );

        $html .= $this->form->input_hidden(
            [
                'name' => 'filterNonce',
                'value' => wp_create_nonce('automotive_filterNonce'),
            ]
        );

        $fieldsArr = [
            [
                'id' => 'makes',
                'title' => 'Make',
                'type' => 'select'
            ],
            [
                'id' => 'bodies',
                'title' => 'Body',
                'type' => 'select'
            ],
            [
                'id' => 'models',
                'title' => 'Model',
                'type' => 'select'
            ],
        ];

        foreach ($fieldsArr as $fieldConf){
            switch($fieldConf['type']){
                case 'select':
                    $html .= $this->setupSelectField($fieldConf['id'], $fieldConf['title']);
                    break;
            }
        }

        $rangeInputAdditionalHtml = sprintf(
            '<br><div class="row"><div class="col-xl-6">%s</div><div class="col-xl-6">%s</div></div>',
            $this->form->input_text(
                'min_price',
                '',
                !empty($_GET) && isset($_GET['price']) ? (string)json_decode($_GET['price'])[0] : '0',
                'min_price',
                'class="text-center"',
                ''
            ),
            $this->form->input_text(
                'max_price',
                null,
                !empty($_GET) && isset($_GET['price']) ? (string)json_decode($_GET['price'])[1] : (get_option('maxPrice') ? : '100000'),
                'max_price',
                'class="text-center"',
                ''
            )
        );

        $html .= sprintf(
            '<div id="_%1$s" class="form-group"><label for="%1$s">%2$s</label><div id="%1$s" class="js-range"%3$s data-min="0" data-max="%4$s" data-name="%1$s" data-mininput="%5$s" data-maxinput="%6$s" data-updatesearch></div>%7$s</div>',
            'price',
            __('Price range', 'automotive'),
            !empty($_GET) && isset($_GET['price']) ? ' data-value="' . $_GET['price'] . '"' : '',
            get_option('maxPrice') ? : '100000',
            'min_price',
            'max_price',
            $rangeInputAdditionalHtml
        );

        $html .= $this->form->form_close();

        return $html;
    }

    private function setupSelectField(string $metaName, string $name = '', bool $showLabel = false){
        $values =['' => !empty($name) ? 'Select '.$name : 'Choose your option'];

        if(!empty($cachedValues = ACFHelper::loadCachedFieldValuesByName($metaName, 'select'))){
            $values = array_merge( $values, $cachedValues);
        }

        ;
        $selectedValue = [];
        if(
            !empty($_GET) &&
            isset($_GET[$metaName]) &&
            array_key_exists( $_GET[$metaName], $values)
        ) {
//            $selectedValue = [
//                strtolower($values[$_GET[$metaName]]) => $values[$_GET[$metaName]]
//            ];
            $selectedValue = strtolower($values[$_GET[$metaName]]);
        }

        return $this->form->input_select(
            $metaName,
            $showLabel ? $name : '',
            !empty($_GET) && isset($_GET[$metaName]) ? $_GET[$metaName] : '',
            $metaName,
            sprintf('class="js-select" data-handle="1"', 'Choose '.$name),
            '',
            !empty($selectedValue) ? $selectedValue : '',
            !empty($values) ? $values : '',
            false
        );
    }
}
