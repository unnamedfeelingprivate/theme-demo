<?php


namespace App\Automotive\Features;


class ACFOptimization {
    public static function all($post_id = '')
    {
        if ( !$post_id ) {
            $post_id = get_the_id();
        }

        $newKey = "acfAllObjects_$post_id";
        $re = get_option($newKey, []);
        return json_decode($re);
    }

    public static function get($key = '', $post_id = '')
    {
        if ( !$post_id ) {
            $post_id = get_the_id();
        }

        if($post_id === 'option') {
            $post_id = 'options';
        }

        $newKey = "acfAllObjects_$post_id";
        $re = get_option($newKey, []);

        $reArray = json_decode($re, true);
        if (! empty($reArray) ) {
            if (isset($reArray[$key])) {
                return $reArray[$key];
            }
        }
        return [];
    }

    public static function updateFieldCache( $post_id ) {
        if (! function_exists('get_fields')) {
            return;
        }

        if (empty($_POST['acf'])) {
            return;
        }
        $re = get_fields($post_id);
        self::setAllObjects($post_id, $re);
    }

    public static function setAllObjects($post_id, $value)
    {
        $jsonValue = json_encode($value);
        $newKey = "acfAllObjects_$post_id";
        //acfAllObjects_options
        if (self::existAllObjects($newKey)) {
            update_option($newKey, $jsonValue, 'no');
        } else {
            add_option($newKey, $jsonValue, '', 'no');
        }
    }

    public static function existAllObjects($key)
    {
        $re = get_option($key, []);
        if (empty($re)) {
            return false;
        }

        return true;
    }
}
