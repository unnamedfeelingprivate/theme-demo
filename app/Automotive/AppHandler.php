<?php


namespace App\Automotive;


use App\Automotive\AbstractHandler;
use App\Automotive\Features\Filters;
use App\Automotive\Helpers\General;
use App\Automotive\Helpers\ACFHelper;
use App\Automotive\Features\ACFOptimization;

class AppHandler extends AbstractHandler {
    public function __construct() {
        $this->handleFilters();
        $this->handleActions();
        $this->registerOptionsPages();

        (new Filters())->run();
    }

    private function handleActions() {
        $addActionsArr = [
            [
                'actionName' => 'acf/save_post',
                'function'   => ['App\Automotive\Features\ACFOptimization', 'updateFieldCache'],
                'priority'   => 1000,
            ],
            [
                'actionName' => 'acf/update_value/key=field_5e8476feaea24',
                'function'   => ['App\Automotive\Helpers\ACFHelper', 'updateSingleFieldCache'],
                'priority'   => 1000,
                'params'     => 3,
            ],
            [
                'actionName' => 'acf/load_field/key=field_5e8476feaea24',
                'function'   => ['App\Automotive\Helpers\ACFHelper', 'loadCachedFieldValues'],
                'priority'   => 1000,
                'params'     => 1,
            ],
            [
                'actionName' => 'acf/update_value/key=field_5e847a6de5ac8',
                'function'   => ['App\Automotive\Helpers\ACFHelper', 'setMaxPriceOption'],
                'priority'   => 900,
                'params'     => 3,
            ],
        ];
        self::addActions($addActionsArr);
    }

    private function handleFilters(){
        add_filter( 'render_block', ['\App\Automotive\Helpers\General', 'wrapGutenbergBlocks'], 10, 2 );
        add_filter( 'acf/fields/google_map/api', ['App\Automotive\Helpers\General', 'setAcfGoogleApiKey'] );
    }

    private function registerOptionsPages(){
        $pages = [
            [
                'page_title' 	=> 'Theme General Settings',
                'menu_title'	=> 'Theme Settings',
                'menu_slug' 	=> 'theme-config',
                'capability'	=> 'edit_posts',
                'redirect'		=> false
            ]
        ];

        if( function_exists('acf_add_options_page') ) {
            foreach ($pages as $page){
                acf_add_options_page($page);
            }
        }
    }
}
