<div class="modal modal-slide modalComponent searchModal js-modal" id="searchModal" aria-hidden="true">
  <div class="modal__overlay" tabindex="-1" data-micromodal-close="">
    <div class="modal__container" role="dialog" aria-modal="true" aria-labelledby="licenseModal">
      <div class="modal__content" id="licenseModal-content">
        <form action="<?= home_url('/') ?>" method="post" class="js-searchForm">
          <div class="searchModal-inputWrapper">
            <label for="searchModal-input">Search</label>
            <input id="searchModal-input" name="s" type="search" placeholder="Start typing here...">
            <button class="btn rounded-right"><i class="icon-search"></i></button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
