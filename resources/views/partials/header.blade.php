<header class="header">
  <div class="container">
    <div class="row header-top">
      <div class="col">
        <p><i class="icon-location"></i> 5404 - 40th Street, High Prairie, AB T0G 1E0</p>
      </div>
      <div class="col-md-1">
        {!! \App\Automotive\Helpers\Templating::outputSocials() !!}
      </div>
    </div>
    <div class="row header-mid">
      <a class="header-logo" href="{{!is_front_page() ? home_url('/') : '#'}}">
        {!! App\Automotive\Helpers\General::remove_width_attribute(wp_get_attachment_image( get_theme_mod( 'custom_logo' ), 'full', false, ['alt' => get_bloginfo(), 'title' => get_bloginfo()] )) !!}
      </a>

      <div class="header-menuToggler">
        <button class="header-menuToggler_btn js-menuToggler"><i class="icon-menu"></i></button>
      </div>

      <div class="header-phones">
        <div class="header-phones_primary">
          <i class="icon-phone"></i>
          <div class="item-wrapper">
            <span>Sales:</span>
            <a href="tel:855-905-2920">855-905-2920</a>
          </div>
        </div>
        <div class="header-phones_secondary">
          <div class="item-wrapper"><span>Service: </span><a href="tel:855-905-2929">855-905-2929</a></div>
          <div class="item-wrapper"><span>Local: </span><a href="tel:780-523-4193">780-523-4193</a></div>
        </div>
      </div>
    </div>
  </div>
  <section class="header-bottom">
    <div class="container">
      <div class="row justify-content-between">
        @if (has_nav_menu('primary_navigation'))
          {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav menuComponent', 'container' => 'nav']) !!}
        @endif

        <div class="header-searchTrigger">
          <button class="header-searchTrigger_btn js-searchTrigger"><i class="icon-search"></i></button>
        </div>

      </div>
    </div>
  </section>
  <div class="scrollFiller"></div>
</header>
