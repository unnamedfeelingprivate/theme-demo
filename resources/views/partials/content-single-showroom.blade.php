<article @php post_class('singleShowroomComponent') @endphp>
  @include('post-types.showroom.parts.pageTitle')
  @include('post-types.showroom.parts.mainSlider')
  @include('post-types.showroom.parts.specs')
  @include('post-types.showroom.parts.content')
  @include('post-types.showroom.parts.interrior360')
  @include('post-types.showroom.parts.features')
  @include('post-types.showroom.parts.performance')
  @include('post-types.showroom.parts.modelRange')
  @include('post-types.showroom.parts.extColors')
  @include('post-types.showroom.parts.nextSteps')
  @include('post-types.showroom.parts.disclaimer')
</article>
