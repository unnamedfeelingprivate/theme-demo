<footer class="footer">
  <div class="container">
    <div class="row footer-top">
      @php dynamic_sidebar('sidebar-footer') @endphp
    </div>
    <div class="row footer-bottom">
      <div class="col-md-9">
        <p>
          © @php(date('Y')) Revolution Ford High Prairie. Proud member of Revolution Auto Group. All rights reserved. Trademarks
          and brands are the property of their respective owners.
        </p>
      </div>
      @if(get_field('general_social_links', 'option'))
        <div class="col-md-3">{!! \App\Automotive\Helpers\Templating::outputSocials() !!}</div>
      @endif
    </div>
  </div>
</footer>

<div class="toTopButton">
  <button class="toTopButton--btn js-toTop hidden"><i class="icon-chevron-thin-up"></i></button>
</div>

<nav class="nav-primary--mobile" id="nav-primary--mobile">
  @if (has_nav_menu('mobile_navigation'))
    {!! wp_nav_menu(['theme_location' => 'mobile_navigation', 'menu_class' => 'nav', 'walker' => new App\Automotive\MenuWalker\MobileMenu()]) !!}
  @endif
</nav>
