<article @php post_class('singleInventoryComponent') @endphp>
  <div class="col-md-9">
    @include('post-types.inventory.content')
    @include('post-types.inventory.tabs')
    @include('post-types.inventory.footerAccordion')
  </div>
  <div class="col-md-3">
    @include('post-types.inventory.sidebar')
  </div>

</article>
