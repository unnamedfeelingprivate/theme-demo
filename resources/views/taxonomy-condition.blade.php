@extends('layouts.app-inventoryArchive')

@section('content')
  @if(have_posts())
    <div class="row">
      @while(have_posts())
        @php(the_post())
        @include('post-types.inventory.loop.loop-inventory')
      @endwhile
    </div>
  @endif
@endsection
