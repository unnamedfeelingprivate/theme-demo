@extends('layouts.app_postSingle')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @switch(get_post_type())
      @case('showroom')
        <div class="container-fluid">
          <main class="main">
            @include('partials.content-single-'.get_post_type())
          </main>
        </div>
        @break

      @default
        <div class="container">
          <main class="main">
            @include('partials.content-single-'.get_post_type())
          </main>
        </div>
    @endswitch
  @endwhile
@endsection
