{{--
Title: Logo slider
Category: common
Icon: admin-comments
Mode: edit
PostTypes: page post
SupportsMode: false
--}}

<div class="logoSlider">
  <button class="glide__arrow glide__arrow--left logoSlider--arrow logoSlider--arrow_left"><span class="js-sliderArrow" data-glide-dir="&lt;"><i class="icon-chevron-thin-left"></i></span></button>
  <div class="logoSlider--wrapper js-logoSlider" data-config='{"perView":{{get_field('perView')}},"focusAt":{{get_field('focusAt')}}}'>
    <div class="glide__track" data-glide-el="track">
      <ul class="glide__slides">
        @if(get_field('logos'))
          @foreach(get_field('logos') as $slide)
          <li class="glide__slide logoSlider--slide">
            <img src="{{wp_get_attachment_image_url($slide, 'full')}}">
          </li>
          @endforeach
        @endif
      </ul>
    </div>
  </div>
  <button class="glide__arrow glide__arrow--right logoSlider--arrow logoSlider--arrow_right"><span class="js-sliderArrow" data-glide-dir="&gt;"><i class="icon-chevron-thin-right"></i></span></button>
</div>

