{{--
Title: Google map
Category: common
Icon: admin-comments
Mode: edit
PostTypes: page post
SupportsMode: false
--}}

<div class="googleMap js-googleMap" id="{{$block['id']}}">
  <script>
    @php
    $config = get_field('coords');

    if(get_field('title')) $config['title'] = get_field('title');
    if(get_field('infowindow_content')) $config['infowindow_content'] = get_field('infowindow_content');
    @endphp

    mapConfig_{{$block['id']}} = @json($config)
  </script>
  <div class="googleMap--container"@if(get_field('height')) style="height: {{get_field('height')}}px"@endif></div>
</div>

