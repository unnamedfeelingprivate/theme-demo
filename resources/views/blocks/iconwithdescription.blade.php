{{--
Title: Icon with description
Category: common
Icon: admin-comments
Mode: edit
PostTypes: page post
SupportsMode: false
--}}

@if(get_field('icon'))
<div class="iconWithDescription" data-{{ $block['id'] }}>
  <div class="iconWithDescription--wrapper">
    <div class="iconWithDescription--icon">
      <img src="{!! wp_get_attachment_image_url(get_field('icon'), 'full') !!}">
    </div>
    @if(get_field('description') !== '')
    <div class="iconWithDescription--description">
      {!! get_field('description') !!}
    </div>
    @endif
  </div>
  <style type="text/css">
    [data-{{$block['id']}}] {
      @if(get_field('background_color'))
      background: {{ get_field('background_color') }};
      @endif
      @if(get_field('text_color'))
      color: {{ get_field('text_color') }};
      @endif
    }
  </style>
</div>
@endif

