{{--
Title: Parallax block
Category: common
Icon: admin-comments
Mode: auto
PostTypes: page post
SupportsMode: false
--}}

@if(get_field('image'))
<div class="js-parallaxBlock parallaxBlock" id="{{$block['id']}}">
  <script>
    var parallaxConfig_{{$block['id']}} = {
      @if(get_field('scale'))
      scale: {{get_field('scale')}},
      @endif
      @if(get_field('overflow'))
      overflow: {{get_field('overflow')}},
      @endif
      @if(get_field('delay'))
      delay: {{get_field('delay')}},
      @endif
      @if(get_field('transition'))
      transition: {{get_field('transition')}},
      @endif
      @if(get_field('maxTransition'))
      maxTransition: {{get_field('maxTransition')}},
      @endif
    }
  </script>
  {!! wp_get_attachment_image(get_field('image'), 'full', false, ['class' => 'js-parallaxTargetImg parallaxTargetImg', 'id' => $block['id']]) !!}
</div>
@endif

