{{--
Title: Testimonial
Category: formatting
Icon: admin-comments
Mode: edit
Align: left
PostTypes: page post
SupportsAlign: left right
SupportsMode: false
--}}

<section data-{{ $block['id'] }}>
  <div class="container">
    <div class="row">
      <div class="col">
        <blockquote class="{{ $block['classes'] }}">
          <p>{{ get_field('testimonial') }}</p>
          <cite>
            <span>{{ get_field('author') }}</span>
          </cite>
        </blockquote>
      </div>
    </div>
  </div>

  <style type="text/css">
    [data-{{$block['id']}}] {
      background: {{ get_field('background_color') }};
      color: {{ get_field('text_color') }};
    }
  </style>
</section>

