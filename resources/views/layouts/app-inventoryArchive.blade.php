<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  <body @php body_class('loading inventoryArchiveComponent') @endphp>
    @include('partials.preloader')
    @php do_action('get_header') @endphp
    <div class="wrap" role="document">
      @include('partials.header')
      <div class="container inventoryArchiveComponent__container">
        <div class="row flex-md-row-reverse">
          <main class="main col-md-6 col-lg-9">
            @yield('content')
          </main>
          @include('post-types.inventory.archive.sidebar')
        </div>
      </div>
      @include('partials.footer')
    </div>
    @include('partials.footer.modals')
    @include('partials.footer.custom')
    @php do_action('get_footer') @endphp
    @php wp_footer() @endphp
  </body>
</html>
