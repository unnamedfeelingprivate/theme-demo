<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  <body @php body_class('loading') @endphp>
    @include('partials.preloader')
    @php do_action('get_header') @endphp
    <div class="wrap" role="document">
      @include('partials.header')
      <main class="main">
        @yield('content')
      </main>
      @include('partials.footer')
    </div>
    @include('partials.footer.modals')
    @include('partials.footer.custom')
    @php do_action('get_footer') @endphp
    @php wp_footer() @endphp
  </body>
</html>
