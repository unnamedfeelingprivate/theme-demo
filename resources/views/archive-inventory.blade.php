@extends('layouts.app-inventoryArchive')

@section('content')
  @include('post-types.inventory.archive.topPanel')
  <div class="row items">
    @if(have_posts())
      @while(have_posts())
        @php(the_post())
        @include('post-types.inventory.loop.loop-inventory')
      @endwhile
    @else
      <div class="col-12">
        <h1>{{__('Nothing found!', 'automotive')}}</h1>
      </div>
    @endif
  </div>
  @include('post-types.inventory.archive.pagination')
@endsection
