<div class="row inventoryArchiveComponent__topPanel">
  <div class="col-md-6 col-lg-4 inventoryArchiveComponent__topPanel-sortItem @if(!have_posts()) d-none @endif ">
    <div id="_orderBy" class="form-group">
      <label class="control-label" for="orderBy">{{__('Order By', 'automotive')}}</label>
      <select name="orderBy" id="orderBy"  class="form-control js-select">
        @foreach($order_config as $order => $title)
          <option value="{{$order}}"{!! ArchiveInventory::isOrderSelected($order) !!}>{{$title}}</option>
        @endforeach
      </select>
    </div>
  </div>
</div>

