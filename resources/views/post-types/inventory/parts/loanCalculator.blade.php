<div class="singleInventoryComponent-sidebar_loanCalcucator js-loanCalculator">
  <div class="title">
    <i class="icon-calculator"></i>
    <h5>Financing calculator</h5>
  </div>
  <div class="row">
    <div class="col-12">
      <!--Amount-->
      <div class="form-group">
        <div class="labeled">Vehicle price <span class="accent">($)</span></div>
        <input
          type="text"
          class="numbersOnly vehicle_price"
          value="{{$meta->sale_price ? $meta->sale_price : $meta->price}}">
      </div>
    </div>

    <div class="col-sm-6">
      <!--Interest rate-->
      <div class="form-group md-mg-rt">
        <div class="labeled">Interest rate <span class="accent">(%)</span></div>
        <input type="text" class="numbersOnly interest_rate" value="">
      </div>
    </div>

    <div class="col-sm-6">
      <!--Period-->
      <div class="form-group md-mg-lt">
        <div class="labeled">Period <span class="accent">(month)</span></div>
        <input type="text" class="numbersOnly period_month" value="">
      </div>
    </div>

    <div class="col-12">
      <!--Down Payment-->
      <div class="form-group">
        <div class="labeled">Down Payment <span class="accent">($)</span></div>
        <input type="text" class="numbersOnly down_payment" value="">
      </div>
    </div>

    <div class="col-12">
      <a href="#" class="button js-calculate_loan_payment">Calculate</a>

      <div class="calculator-alert alert alert-danger">

      </div>
    </div>

    <!--Results-->
    <div class="col-md-12">
      <div class="stm-calculator_results">
        <div class="stm-calculator_results-inner">
          <div class="stm-calc-label">Biweekly Payment</div>
          <div class="biweekly_payment h5"></div>

          <div class="stm-calc-label">Monthly Payment</div>
          <div class="monthly_payment h5"></div>

          <div class="stm-calc-label">Total Interest Payment</div>
          <div class="total_interest_payment h5"></div>

          <div class="stm-calc-label">Total Amount to Pay</div>
          <div class="total_amount_to_pay h5"></div>
        </div>
      </div>
    </div>
  </div>
</div>
