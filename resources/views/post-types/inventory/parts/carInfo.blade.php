<div class="singleInventoryComponent-sidebar_carInfo">
  @if($meta->vin_number && ($meta->price || $meta->sale_price))
  <div class="carproof-badge" data-vin="{{$meta->vin_number}}"></div>

  <div class="cargurus_wrapper cg_style">
    <span data-cg-vin="{{$meta->vin_number}}" data-cg-price="{{ $meta->sale_price ?  $meta->sale_price :  $meta->price}}"></span>
  </div>
  @endif

  <ul>
    @foreach(['year', 'trims', 'drivetrains', 'transmission', 'fuel_types', 'odometer', 'vin_number'] as $info)
      @if($meta->$info)
        <li class="item">
          <h5>{{get_field_object($info)['label']}}</h5>
          <span>{{$meta->$info}}</span>
        </li>
      @endif
    @endforeach
  </ul>
</div>
