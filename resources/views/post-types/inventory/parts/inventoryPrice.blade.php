@if($meta->price)
  <div class="carPrices">
    <div class="carPrices-main">
      <div class="carPrices-regular @if($meta->sale_price) with-discount @endif">
        @if($meta->regular_price_label)
          <span class="label">{{$meta->regular_price_label}}</span>
        @else
          <span class="label">{{__('MSRP', 'automotive')}}</span>
        @endif
        <span class="amount">${{number_format($meta->price)}}</span>
      </div>
      @if($meta->sale_price)
        <div class="discountPrice">
          <span class="label">{{ __('Sale price', 'automotive') }}</span>
          <span class="amount">{{number_format($meta->sale_price)}}</span>
        </div>
      @endif
    </div>

    <div class="carPrices-footer">
      @if($meta->price && $meta->sale_price)
        {{__('Instant saving', 'automotive')}} ${{number_format((int)$meta->price - (int)$meta->sale_price)}}
      @else
        {{__('Price includes freight and other charges', 'automotive')}}
      @endif
    </div>
  </div>
@endif
