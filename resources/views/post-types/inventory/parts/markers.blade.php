@if($meta->vin_number || $meta->stock_number)
  <ul class="singleInventoryComponent-markers">
    @if($meta->stock_number)
      <li class="stockNum">Stock # <span>{{$meta->stock_number}}</span></li>
    @endif

    @if($meta->vin_number)
      <li class="windowSticker">
        <a href="http://www.windowsticker.forddirect.com/windowsticker.pdf?vin={{$meta->vin_number}}" target="_blank">
          <i class="icon-label-reverse"></i>
          {{__('Window Sticker', 'automotive')}}
        </a>
      </li>
    @endif
  </ul>
@endif
