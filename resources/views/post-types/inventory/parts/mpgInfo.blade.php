@if($meta->city_mpg && $meta->highway_mpg)
<div class="singleInventoryComponent-sidebar_mpgInfo">
  <div class="mpg-itm">
    <span class="label">{{$meta->city_mpg}}</span>
    <span class="descr">{{__('MPG, city', 'automotive')}}</span>
  </div>
  <div class="mpg-itm with-icon">
    <i class="icon-fuel"></i>
  </div>
  <div class="mpg-itm">
    <span class="label">{{$meta->highway_mpg}}</span>
    <span class="descr">{{__('MPG, highway', 'automotive')}}</span>
  </div>
</div>
@endif
