@if($meta->image_gallery)
  <section class="singleInventoryComponent-gallery">
    @if(count($meta->image_gallery) > 1)
    <div class="glide js-inventoryCarousel" data-confname="singleInventory" data-useslider="{{(int) count($meta->image_gallery) > 1}}">
      <button class="glide__arrow glide__arrow--left singleInventoryComponent-gallery--arrow singleInventoryComponent-gallery--arrow_left"><span class="js-sliderArrow" data-glide-dir="&lt;"><i class="icon-chevron-thin-left"></i></span></button>
        <div class="glide__track" data-glide-el="track">
          <ul class="glide__slides">
            @foreach($meta->image_gallery as $image)
              <li class="glide__slide">
                <img src="{{$image->sizes->large}}" alt="{{$post->post_title}} - {{$image->title}}" data-src="{{$image->url}}">
              </li>
            @endforeach
          </ul>
        </div>
      <button class="glide__arrow glide__arrow--right singleInventoryComponent-gallery--arrow singleInventoryComponent-gallery--arrow_right"><span class="js-sliderArrow" data-glide-dir="&gt;"><i class="icon-chevron-thin-right"></i></span></button>
    </div>
    @else
    <div class="singleInventoryComponent-gallery--wrapper js-lightGallery">
      @foreach($meta->image_gallery as $image)
        <a href="{{$image->url}}">
          <img src="{{$image->sizes->large}}" alt="{{$post->post_title}} - {{$image->title}}">
        </a>
      @endforeach
    </div>
    @endif
  </section>
@endif
