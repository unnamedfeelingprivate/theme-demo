<div class="singleInventoryComponent-sidebar_buttons">
  <a href="/financing">Get pre-approved <i class="icon-cash"></i></a>
  <a href="#footerAccordions" data-openaccordion="dealershipInfo">Quick quote <i class="icon-info-circle"></i></a>
  <a href="#footerAccordions" data-openaccordion="tradeIn">Trade-in form <i class="icon-trade"></i></a>
  <a href="#footerAccordions" data-openaccordion="testDrive">Schedule Test Drive <i class="icon-steering_wheel"></i></a>
</div>
