<section class="accordion tabsComponent js-tabsComponent">
  <nav class="accordion-tabs">
    @if($post->post_content)
      <button class="accordion-tab" data-actab-group="0" data-actab-id="0"> @php(_e('Vechile Overview', 'automotive')) </button>
    @endif
    <button class="accordion-tab" data-actab-group="0" data-actab-id="1">@php(_e('Technical Info', 'automotive'))</button>
    @if($meta->additional_features)
      <button class="accordion-tab" data-actab-group="0" data-actab-id="2">@php(_e('Features', 'automotive'))</button>
    @endif
  </nav>
  <section class="accordion-content">
    @if($post->post_content)
    <article class="accordion-item" data-actab-group="0" data-actab-id="0">
      <h4 class="accordion-item__label">@php(_e('Vechile Overview', 'automotive'))</h4>
      <div class="accordion-item__container">
        {!! the_content() !!}
      </div>
    </article>
    @endif


    <article class="accordion-item" data-actab-group="0" data-actab-id="1">
      <div class="accordion-item__container">
        <div class="techInfoBlocks">
          <div class="techInfoBlocks-block">
            <h4 class="techInfoBlocks-title"><i class="icon-speedometr3"></i> General Info</h4>
            <ul class="techInfoBlocks-content">
              @foreach(['bodies', 'drivetrains', 'number_of_doors', 'exterior_colors', 'interior_colors', 'vin_number'] as $key => $field)
                @if(property_exists($meta, $field) && $meta->$field)
                <li class="item">
                  <h5>{{get_field_object($field)['label']}}</h5>
                  <span>{{$meta->$field}}</span>
                </li>
                @endif
              @endforeach
            </ul>
          </div>
          <div class="techInfoBlocks-block">
            <h4 class="techInfoBlocks-title"><i class="icon-engine"></i> Engine</h4>
            <ul class="techInfoBlocks-content">
              @foreach(['engine_description', 'engines', 'number_of_cylinders', 'fuel_types'] as $key => $field)
                @if(property_exists($meta, $field) && $meta->$field)
                <li class="item">
                  <h5>{{get_field_object($field)['label']}}</h5>
                  <span>{{$meta->$field}}</span>
                </li>
                @endif
              @endforeach
            </ul>
          </div>
          <div class="techInfoBlocks-block">
            <h4 class="techInfoBlocks-title"><i class="icon-transmission2"></i> Transmission</h4>
            <ul class="techInfoBlocks-content">
              @foreach(['transmission', 'transmission_description'] as $key => $field)
                @if(property_exists($meta, $field) && $meta->$field)
                <li class="item">
                  <h5>{{get_field_object($field)['label']}}</h5>
                  <span>{{$meta->$field}}</span>
                </li>
                @endif
              @endforeach
            </ul>
          </div>
        </div>
      </div>
    </article>


    @if($meta->additional_features)
      <article class="accordion-item" data-actab-group="0" data-actab-id="2">
        <h4 class="accordion-item__label">@php(_e('Features', 'automotive'))</h4>
        <div class="accordion-item__container accordion-item__container-features">
          <ul>
            @foreach($meta->additional_features as $key => $feature)
              @if($key%2 == 0)<li><i class="icon-check-circle"></i> {{$feature->label}}</li>@endif
            @endforeach
          </ul>
          <ul>
            @foreach($meta->additional_features as $key => $feature)
              @if($key%2 != 0)<li><i class="icon-check-circle"></i> {{$feature->label}}</li>@endif
            @endforeach
          </ul>
        </div>
      </article>
    @endif
  </section>
</section>
