<header class="singleInventoryComponent-header">
  <h1 class="singleInventoryComponent-title">{!! get_the_title() !!}</h1>

  @include('post-types.inventory.parts.markers')
</header>


@include('post-types.inventory.parts.carousel')
