<aside class="singleInventoryComponent-sidebar">
  @include('post-types.inventory.parts.inventoryPrice')
  @include('post-types.inventory.parts.buttons')
  @include('post-types.inventory.parts.carInfo')
  @include('post-types.inventory.parts.mpgInfo')
  @include('post-types.inventory.parts.loanCalculator')
</aside>
