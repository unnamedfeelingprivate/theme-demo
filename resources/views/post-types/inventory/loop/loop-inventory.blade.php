<article class="col-md-4 loopInventory">
  @php($meta = App\Automotive\Features\ACFOptimization::all($post->ID))
  <div class="loopInventory__content">
    <header class="loopInventory__header">
      <a href="{{get_the_permalink($post->ID)}}" title="{{$post->post_title}}">
        @if($meta->image_gallery)
        {!! App\Automotive\Helpers\General::remove_width_attribute(wp_get_attachment_image($meta->image_gallery[0]->ID, 'medium')) !!}
        @else
          <img src="{{get_field('default_inventory_image', 'option')['sizes']['medium']}}" alt="">
        @endif
      </a>
    </header>
    <a href="{{get_the_permalink($post->ID)}}" title="{{$post->post_title}}">
      <h3 class="loopInventory__title">{!! $post->post_title !!}</h3>
    </a>

    @if($meta->price)
      <section class="loopInventory__prices">
        <div class="loopInventory__price @if($meta->sale_price) oldprice @endif">
          $ {{number_format($meta->price)}}
        </div>

        @if($meta->sale_price)
          <div class="loopInventory__price">
            $ {{number_format($meta->sale_price)}} *
          </div>
        @endif
      </section>
    @endif

    @if($meta->odometer || $meta->transmission || $meta->drivetrains || $meta->engines || $meta->stock_number)
      <section class="loopInventory__options">
        @if($meta->odometer)
          <span class="loopInventory__option"><i class="icon-road"></i>{{$meta->odometer}}</span>
        @endif
        @if($meta->transmission)
          <span class="loopInventory__option"><i class="icon-transmission"></i>{{$meta->transmission}}</span>
        @endif
        @if($meta->drivetrains)
          <span class="loopInventory__option"><i class="icon-drive_2"></i>{{$meta->drivetrains}}</span>
        @endif
        @if($meta->engines)
          <span class="loopInventory__option"><i class="icon-engine_fill"></i>{{$meta->engines}}</span>
        @endif
        @if($meta->stock_number)
          <span class="loopInventory__option"><i class="icon-label-reverse"></i>Stock №{{$meta->stock_number}}</span>
        @endif
      </section>
    @endif

    <footer class="loopInventory__footer">
      <a href="{{get_the_permalink($post->ID)}}" title="{{$post->post_title}}" class="loopInventory__footer-link">
        Get a quote <i class="icon-angle-round"></i>
      </a>
    </footer>
  </div>
</article>
