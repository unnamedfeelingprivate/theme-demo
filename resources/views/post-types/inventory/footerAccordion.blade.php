<section class="footerAccordions">
  <div class="js-collapsible collapsible" data-accordionid="dealershipInfo" data-accordiongroup="singleInventoryFooter" data-activeonload="true" data-allowclose="false">
    <a href="#" class="collapsibleTrigger js-collapsibleTrigger">{{__('Dealership info', 'automotive')}}</a>
    <div class="collapsibleContent hidden" data-height="180px">
      <div class="row">
        <div class="col-md-4">
          <div class="iconWithLabel">
            <i class="icon-performance"></i>
            <span class="label">Revolution Ford HP</span>
          </div>
          <p style="color:#888;font-size:13px">This vehicle can be delivered to any of Revolution dealerships at no additional cost.</p>
          <div class="iconWithLabel">
            <i class="icon-pin"></i>
            <span class="label">5404 – 40th Street<br>
            High Prairie, AB T0G 1E0</span>
          </div>
          <div class="iconWithLabel">
            <i class="icon-phone"></i>
            <span class="label">PHONE: <br><a href="tel:855-905-2920">855-905-2920</a></span>
          </div>
        </div>
        <div class="col-md-8">
          <div class="iconWithLabel">
            <i class="icon-paper-plane"></i>
            <span class="label">Quick Quote</span>
          </div>
          {!! do_shortcode('[hf_form slug="quick-quote-form"]') !!}
        </div>
      </div>
    </div>
  </div>
  <div class="js-collapsible collapsible" data-accordionid="testDrive" data-accordiongroup="singleInventoryFooter" data-allowclose="false">
    <a href="#" class="collapsibleTrigger js-collapsibleTrigger">{{__('Test Drive Request', 'automotive')}}</a>
    <div class="collapsibleContent hidden" data-height="180px">
      <div class="row">
        <div class="col-12">
          {!! do_shortcode('[hf_form slug="test-drive-request-form"]') !!}
        </div>
      </div>
    </div>
  </div>
  <div class="js-collapsible collapsible" data-accordionid="tradeIn" data-accordiongroup="singleInventoryFooter" data-allowclose="false">
    <a href="#" class="collapsibleTrigger js-collapsibleTrigger">Trade In Form</a>
    <div class="collapsibleContent hidden" data-height="180px">
      <div class="row">
        <div class="col-12">
          {!! do_shortcode('[hf_form slug="trade-in-form"]') !!}
        </div>
      </div>
    </div>
  </div>
</section>

