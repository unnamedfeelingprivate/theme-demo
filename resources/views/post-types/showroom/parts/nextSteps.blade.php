@if($meta->next_steps && count($meta->next_steps) > 0)
  <section class="singleShowroomComponent-nextSteps">
    <div class="container">
      <div class="row">
        <div class="singleShowroomComponent-nextSteps_title">
          <h3>Next Steps</h3>
        </div>
        @foreach($meta->next_steps as $step)
          <div class="col-md-4 step">
            <div class="step-content">
              @if($step->icon)
                <div class="step-image">
                  {!! App\Automotive\Helpers\General::remove_width_attribute(wp_get_attachment_image($step->icon->ID, 'small')) !!}
                </div>
              @endif
              @if($step->title)
                <h4 class="step-title">
                  @if($step->link)
                    <a href="{!! $step->link !!}" target="_blank">{{$step->title}}</a>
                  @else
                  {{$step->title}}
                  @endif
                </h4>
              @endif
            </div>
          </div>
        @endforeach
      </div>
    </div>
  </section>
@endif
