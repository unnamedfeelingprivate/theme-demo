@if($meta->model_range && count($meta->model_range) > 0)
<section class="singleShowroomComponent-modelRange">
  <div class="container">
    <div class="row">
      <div class="singleShowroomComponent-modelRange_title">
        <h3>Model range</h3>
      </div>

      @foreach($meta->model_range as $model)
        <div class="col-md-6 col-lg-4 model">
          <div class="model-content">
            <h5 class="model-title">{{$model->post_title}}</h5>
            @if(has_post_thumbnail($model->ID))
              <div class="model-image">{!! get_the_post_thumbnail((int)$model->ID, 'large') !!}</div>
            @endif

            @if(App\Templating::getField('additional_features', (int)$model->ID))
            <div class="model-description">
              <ul>
                @foreach(App\Templating::getField('additional_features', (int)$model->ID) as $key => $feature)
                  <li><i class="icon-check-circle"></i> {{$feature['label']}}</li>
                @endforeach
              </ul>
            </div>
            @endif

            @if(App\Templating::getField('price', (int)$model->ID))
            <div class="model-price">
              <p>Well-equipped from</p>
              {!! number_format(App\Templating::getField('price', (int)$model->ID)) !!}
            </div>
            @endif
          </div>
        </div>
      @endforeach

    </div>
  </div>
</section>
@endif
