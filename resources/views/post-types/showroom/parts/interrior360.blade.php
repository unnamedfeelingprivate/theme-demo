@if($meta->interior_pano_iframe_link)
<section class="singleShowroomComponent-interrior360">
  <div class="container">
    <div class="row">
      <div class="singleShowroomComponent-interrior360 singleShowroomComponent-interrior360_title">
        <h3>Interior 360</h3>
      </div>
      <div class="col-12">
        <div
          class="js-lazyloadIframe"
          data-lazyload-src="{{$meta->interior_pano_iframe_link}}"
          data-type="iframe"
          @if($meta->pano_height) data-height="{{$meta->pano_height}}" @endif
          ></div>
      </div>
    </div>
  </div>
</section>
@endif
