@if($meta->performance_highlights && count($meta->performance_highlights) > 0)
  <section class="singleShowroomComponent-performanceHighlights">
    <div class="container">
      <div class="row">
        <div class="singleShowroomComponent-performanceHighlights_title">
          <h3>Performance</h3>
        </div>
        @foreach($meta->performance_highlights as $highlight)
          <div class="col-md-5 highlight">
            <div class="highlight-content">
              @if($highlight->image)
                <div class="highlight-image">
                  {!! App\Automotive\Helpers\General::remove_width_attribute(wp_get_attachment_image($highlight->image->ID, 'large')) !!}
                </div>
              @endif
              @if($highlight->title) <h4 class="highlight-title">{{$highlight->title}}</h4> @endif
              @if($highlight->description) <div class="highlight-description">{!! $highlight->description !!}</div> @endif
            </div>
          </div>
        @endforeach
      </div>
    </div>
  </section>
@endif
