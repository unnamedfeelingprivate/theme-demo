@if($meta->exterior_colors && count($meta->exterior_colors->colors) > 0)
  <section class="singleShowroomComponent-extColors">
    <div class="container">
      <div class="row">
        <div class="singleShowroomComponent-extColors_title">
          <h3>Exterior colors</h3>
        </div>

        @foreach($meta->exterior_colors->colors as $color)
          <div class="col-md-2 color">
            <div class="color-content">
              @if($color->image)
                <div class="color-image">
                  {!! App\Automotive\Helpers\General::remove_width_attribute(wp_get_attachment_image($color->image->ID, 'small')) !!}
                </div>
              @endif
              @if($color->title)
                <h4 class="color-title">
                  {{$color->title}}
                </h4>
              @endif
            </div>
          </div>
        @endforeach

        @if($meta->exterior_colors->description)
        <div class="col-12 disclaimer">
          {!! $meta->exterior_colors->description !!}
        </div>
        @endif
      </div>
    </div>
  </section>
@endif
