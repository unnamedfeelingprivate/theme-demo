@if($meta->specs && count($meta->specs) > 0)
@php
$className = 4;
$columns = 3;
$elements = count($meta->specs);

if($elements > 3){
    $className = (int) floor(12 / $elements);
}

$className = $className < 2 ? 2 : $className;
@endphp
<section class="singleShowroomComponent-specs">
  <div class="container">
    <div class="row justify-content-center">
      @if(count($meta->specs) > 0)
        @foreach($meta->specs as $spec)
          <div class="col-md-{{$className}} spec">
          @if($spec->icon)
            <div class="spec-icon">
              {!! App\Automotive\Helpers\General::remove_width_attribute(wp_get_attachment_image(json_encode($spec->icon->id))) !!}
            </div>
          @endif
          @if($spec->name)
            <div class="spec-title">{{$spec->name}}</div>
          @endif
          @if($spec->value_prepend)
            <div class="spec-prepend">{{$spec->value_prepend}}</div>
          @endif
          @if($spec->value)
            <div class="spec-value">{{$spec->value}}</div>
          @endif
          @if($spec->description)
            <div class="spec-description">{{$spec->description}}</div>
          @endif
          </div>
        @endforeach
      @endif
    </div>
  </div>
</section>
@endif
