@if($meta->feature_highlights && count($meta->feature_highlights) > 0)
<section class="singleShowroomComponent-features">
  <div class="container">
    <div class="row">
      <div class="singleShowroomComponent-features_title">
        <h3>Feature Highlights</h3>
      </div>
      @foreach($meta->feature_highlights as $feature)
      <div class="col-md-4 feature">
        <div class="feature-content">
          @if($feature->image)
            <div class="feature-image">
              {!! App\Automotive\Helpers\General::remove_width_attribute(wp_get_attachment_image($feature->image->ID, 'medium')) !!}
            </div>
          @endif
          @if($feature->title) <h4 class="feature-title">{{$feature->title}}</h4> @endif
          @if($feature->description) <div class="feature-description">{!! $feature->description !!}</div> @endif
        </div>
      </div>
      @endforeach
      <div class="col-12 disclaimer">
        <p>All vehicle and feature images are for illustrative purposes only.</p>
      </div>
    </div>
  </div>
</section>
@endif
