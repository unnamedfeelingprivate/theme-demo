@if($meta->main_gallery || get_post_thumbnail_id() || $meta->main_slider)
  <section class="singleShowroomComponent-mainSlider">
    @switch(true)
      @case ($meta->main_slider)
        {!! do_shortcode('[smartslider3 slider='.$meta->main_slider.']') !!}
        @break

      @case($meta->main_gallery && count($meta->main_gallery) > 1)
        <div class="glide js-showroomSlider" data-confname="singleShowroom" data-useslider="{{(int) count($meta->main_gallery) > 1}}">
          <button class="glide__arrow glide__arrow--left singleInventoryComponent-gallery--arrow singleInventoryComponent-gallery--arrow_left"><span class="js-sliderArrow" data-glide-dir="&lt;"><i class="icon-chevron-thin-left"></i></span></button>
          <div class="glide__track" data-glide-el="track">
            <ul class="glide__slides">
              @foreach($meta->main_gallery as $image)
                <li class="glide__slide">
                  <img src="{{$image->sizes->large}}" alt="{{$post->post_title}} - {{$image->title}}" data-src="{{$image->url}}">
                </li>
              @endforeach
            </ul>
          </div>
          <button class="glide__arrow glide__arrow--right singleInventoryComponent-gallery--arrow singleInventoryComponent-gallery--arrow_right"><span class="js-sliderArrow" data-glide-dir="&gt;"><i class="icon-chevron-thin-right"></i></span></button>
        </div>
        @break

      @case($meta->main_gallery && count($meta->main_gallery) == 1)
        @foreach($meta->main_gallery as $image)
          <img src="{{$image->sizes->showroom-main}}" alt="{{$post->post_title}} - {{$image->title}}" data-src="{{$image->url}}">
        @endforeach
        @break

      @default
        {!! the_post_thumbnail( 'showroom-main' ) !!}
    @endswitch
  </section>
@endif
