@if(get_field('showroom_disclaimer', 'option') || $meta->disclaimer)
  <section class="singleShowroomComponent-disclaimer">
    <div class="container">
      <div class="row">
        <div class="singleShowroomComponent-disclaimer_title">
          <h3>Disclaimer</h3>
        </div>
        <div class="col-12 singleShowroomComponent-disclaimer_content">
          @switch(true)
            @case($meta->disclaimer)
              {!! $meta->disclaimer !!}
              @break

            @case(get_field('showroom_disclaimer', 'option'))
              {!! get_field('showroom_disclaimer', 'option') !!}
              @break
          @endswitch
        </div>
      </div>
    </div>
  </section>
@endif
