import 'lightgallery.js'
import 'lightgallery.js/lib/js/lg-utils'

let lightGalleryComponent = new function () {
  let self = this

  self.config = {
    download: false,
    thumbnail:true,
    animateThumb: false,
    showThumbByDefault: false,
  }

  self.initLGinInventorySlider = slider => {
    let conf = self.config
    conf.selector = 'li:not(.glide__slide--clone) img'
    lightGallery(slider.querySelector('.glide__slides'), conf)
  }

  self.initLGonElement = selector => {
    let conf = self.config
    conf.selector = 'a'
    lightGallery(document.querySelector(selector), conf)
  }
};

export default lightGalleryComponent
