import {Loader, LoaderOptions} from 'google-maps';

let mapsComponent = new function() {
  let self = this

  self.init = () => {
    let blocks = document.querySelectorAll('.js-googleMap')

    if (blocks && blocks.length){
      self.handleMapBlocks(blocks)
    }
  }

  self.handleMapBlocks = blocks => {
    const automotiveVars = window.automotiveVars || {}
    const options = {/* todo */};

    if (!automotiveVars.googleApiKey) return false;

    const loader = new Loader(automotiveVars.googleApiKey, options);

    loader.load().then(google => {
      blocks.forEach(block => {
        const config = window['mapConfig_' + block.id]

        if (config){
          let mapconfig = {
            center: {
              lat: config.lat,
              lng: config.lng,
            },
            zoom: config.zoom
          }
          const map = new google.maps.Map(block.querySelector('.googleMap--container'), mapconfig);

          let marker = new google.maps.Marker({
            position: mapconfig.center,
            map: map,
            title: config.title || 'Our office'
          });

          if (config.infowindow_content !== undefined){
            let infowindow = new google.maps.InfoWindow({
              content: '<h5>' + config.title + '</h5>' + config.infowindow_content
            });

            marker.addListener('click', function() {
              infowindow.open(map, marker);
            });
          } else {
            marker.setMap(map);
          }

        }
      })
    })
  }

};

export default mapsComponent

