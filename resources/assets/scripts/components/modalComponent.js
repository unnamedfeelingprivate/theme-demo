import MicroModal from 'micromodal'

let modalComponent = new function(){
  let self = this

  self.micromodal = MicroModal

  self.modals = document.querySelectorAll('.js-modal')
  self.config = {
    // onClose: modal => {
    //   modal.classList.add('is-closing')
    //
    //   setTimeout(modal => {
    //     modal.classList.remove('is-closing')
    //   }, 500, modal)
    // },
    disableScroll: true,
    awaitOpenAnimation: true,
    awaitCloseAnimation: true
  }

  self.init = () => {
    self.instance = MicroModal.init(self.config)
  }
};

export default modalComponent
