import preloaderComponent from '../components/preloaderComponent'
import collapsibleComponent from './collapsibleComponent'

let urlHandlerComponent = new function() {
  let self = this

  self.init = () => {
    let links = document.querySelectorAll('a')

    links.forEach(link => {
      link.addEventListener('click', self.onClick)
    })
  }

  self.onLoad = () => {
    if(window.location.hash) {
      self.handleHashTransition(window.location.hash)
    }
  }

  self.onClick = event => {
    let trg = event.target.nodeName === 'A' ? event.target : event.target.closest('a'),
      containsExcludedClasses = [].slice.apply(trg.classList).some(name => ['page-numbers'].includes(name)),
      hash = trg.href ? trg.href.split('#')[1] : ''
    ;

    if(!containsExcludedClasses){
      preloaderComponent.showPreloader()
    }

    let canHandle = trg.nodeName === 'A'
      && !containsExcludedClasses
      && trg.closest('.wc-tabs') === null
      && trg.href !== window.location.href
      && trg.target === ''
    ;

    if (hash){
      event.preventDefault()
      if (trg.dataset && trg.dataset.openaccordion) {
        self.openTargetAccordion(trg)
      }

      self.handleHashTransition(hash)
    }

    if(canHandle){
      preloaderComponent.showPreloader()
    }
  }

  self.handleHashTransition = (hash = '') => {
    console.log(hash)
    if(hash !== ''){
      let targetElem = document.querySelector(hash) ?
        document.querySelector(hash) :
        document.querySelector('.' + hash.replace('#', ''));

      if (targetElem) targetElem.scrollIntoView({behavior: 'smooth', block: 'start', inline: 'nearest'});

      return false
    }

    // return true
  }

  self.openTargetAccordion = link => {
    if(link.dataset && link.dataset.openaccordion){
      console.log('.js-collapsible[data-accordionid="' + link.dataset.accordionid + '"]')
      let accordion = document.querySelector('.js-collapsible[data-accordionid="' + link.dataset.openaccordion + '"]')

      console.log(accordion)

      if (!accordion) return false;

      let trigger = accordion.querySelector('.js-collapsibleTrigger'),
        content = accordion.querySelector('.collapsibleContent')

      collapsibleComponent.openContent(content, trigger)
      if (collapsibleComponent.isInGroup(content.parentNode)) collapsibleComponent.closeOther(content.parentNode)
    }
  }
};

export default urlHandlerComponent
