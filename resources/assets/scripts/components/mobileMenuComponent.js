import Mmenu from 'mmenu-js/src/mmenu'

let mobileMenuComponent = new function(){
  let self = this

  self.status = false

  self.config = {
    'extensions': [
      'pagedim-black',
      'theme-dark',
    ],
  }

  self.init = () => {
    self.mainMenu = document.querySelector('#nav-primary--mobile')
    self.togglers = document.querySelectorAll('.js-menuToggler')

    if(!self.status){
      self.status = true
      self.initMmenu()
      self.initTogglers()
    }
  }

  self.initTogglers = () => {
    if (self.togglers){
      self.togglers.forEach(btn => {
        btn.addEventListener('click', () => {
          self.mMenuApi.open('#nav-primary--mobile')
        })
      })
    }
  }

  self.initMmenu = () => {
    self.mMenuInstance = new Mmenu(self.mainMenu, self.config)

    self.mMenuApi = self.mMenuInstance.API
  }
};

export default mobileMenuComponent


