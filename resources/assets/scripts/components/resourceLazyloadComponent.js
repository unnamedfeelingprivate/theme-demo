let resourceLazyloadComponent = new function () {
  let self = this

  self.config = {
    // root: document.body,
    rootMargin: '200px',
    threshold: 1.0,
  }

  self.init = (selector = null, params = {}) => {
    if(!selector) return false;

    self.elements = document.querySelectorAll(selector)

    if (!self.elements) return false;

    self.elements.forEach(element => self.observer.observe(element))
  }

  self.observer = new IntersectionObserver(entries => {
    entries.forEach(e => {
      if(e.isIntersecting) {
        let target = e.target,
          type = target.nodeName === 'DIV' ? target.dataset.type : target.nodeName.toLowerCase()

        switch (type) {
          case ('iframe'):
            self.loadIframe(target)
            break;

          default:
            self.loadImage(target)
        }
      }
    });
  }, self.config);

  self.loadIframe = target => {
    if(target.dataset.lazyloadSrc && target.dataset.lazyloadSrc !== '') {
      let iframe = document.createElement('iframe')
      iframe.src = target.dataset.lazyloadSrc
      iframe.style = 'width:100%;height:' + (target.dataset.height ? target.dataset.height : 300) + 'px'
      iframe.frameBorder = '0'
      target.appendChild(iframe)
      target.classList.add('observed')
      self.observer.unobserve(target)
    }
  }

  self.loadImage = target => {
    if(target.attributes && target.attributes.length){
      for (var i = 0; i < target.attributes.length; i++) {
        let attr = target.attributes[i]
        if (/^data-lazyload-/.test(attr.nodeName)) {
          target[attr.nodeName.replace(/^data-lazyload-/, '')] = attr.nodeValue
        }
      }
    }
  }
};

export default resourceLazyloadComponent
