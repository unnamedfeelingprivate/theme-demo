import noUiSlider from 'nouislider';
import filterComponent from './filterComponent';
import searchUtils from '../util/searchUtils'

let rangeSliderComponent = new function(){
  let self = this

  self.init = () => {
    self.sliders = self.getSliders('.js-range')

    if(self.sliders){
      self.sliders.forEach(slider => {
        let searchFieldData = searchUtils.getSearchItem(slider.id),
          startConf = [parseInt(slider.dataset.min), parseInt(slider.dataset.max)]

        if(searchFieldData){
          startConf = JSON.parse(searchFieldData)
        }

        let instance = noUiSlider.create(slider, {
          start: startConf,
          connect: true,
          range: {
            'min': parseInt(slider.dataset.min),
            'max': parseInt(slider.dataset.max),
          },
        });

        if (slider.dataset.mininput && slider.dataset.maxinput) self.bindInputs(instance, slider)
      })
    }
  }

  self.getSliders = (selector = null) => {
    if(!selector) return console.warn('No selector provided for noUiSlider!');

    return document.querySelectorAll(selector)
  }

  self.bindInputs = (instance, slider) => {
    let mininput = slider.parentNode.querySelector('[name="' + slider.dataset.mininput + '"]'),
      maxinput = slider.parentNode.querySelector('[name="' + slider.dataset.maxinput + '"]')

    if(mininput && maxinput) {
      instance.on('slide', event => {
        mininput.value = parseInt(event[0])
        maxinput.value = parseInt(event[1])
      })

      instance.on('start', () => {
        if(filterComponent.config.activeRequest) filterComponent.abortCurrentFetch()
      })

      if(slider.dataset.updatesearch !== undefined){
        instance.on('set', event => {
          let arr = [parseInt(event[0]),parseInt(event[1])]
          searchUtils.updateSearch(slider.id, JSON.stringify(arr))
          // setTimeout(() => {
          //
          // })
        })
      }

    }

  }
};

export default rangeSliderComponent

