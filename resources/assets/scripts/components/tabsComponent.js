let tabsComponent = new function () {
  let self = this;
  self.components = document.querySelectorAll('.js-tabsComponent')

  self.handleShow = component => {
    let items = component.querySelectorAll('.accordion-item'),
        buttons = component.querySelectorAll('.accordion-tab')

    if(buttons){
      buttons.forEach(button => {
        button.addEventListener('click', event => {
          let groupId = button.dataset.actabGroup,
              tabId = button.dataset.actabId

          items.forEach(item => {
            if(groupId === item.dataset.actabGroup){
              if(tabId === item.dataset.actabId){
                item.classList.add('accordion-active')
              } else {
                item.classList.remove('accordion-active')
              }
            }
          })

          buttons.forEach(btn => btn.classList.remove('accordion-active'))

          button.classList.add('accordion-active')
        })
      })
    }
  }

  self.init = () => {
    if (self.components) {
      self.components.forEach(component => {
        component.querySelectorAll('.accordion-item') && component.querySelectorAll('.accordion-item')[0].classList.add('accordion-active')
        component.querySelectorAll('.accordion-tab') && component.querySelectorAll('.accordion-tab')[0].classList.add('accordion-active')
        self.handleShow(component)
      })
    }
  }
};

export default tabsComponent
