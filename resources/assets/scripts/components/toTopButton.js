import calcViewport from '../util/calcViewport'

let toTopButtonComponent = new function(){
  let self = this

  self.toTopArrow = document.querySelector('.js-toTop')
  self.scrolling = false
  self.checkerFunc = (elem) => {
    if(elem){
      let viewport = calcViewport.calc()

      if(window.scrollY < viewport.height){
        elem.classList.add('hidden')
      } else {
        elem.classList.remove('hidden')
      }
    }
  }

  self.init = () => {
    setInterval( () => {
      if ( self.scrolling ) {
        self.scrolling = false;
        self.checkerFunc(self.toTopArrow)
      }
    }, 550 );

    self.toTopArrow.addEventListener('click', self.onClick)
  }

  self.onScroll = () => {
    self.scrolling = true;
  }

  self.onClick = () => window.scrollTo({ top: 0, behavior: 'smooth' })
};

export default toTopButtonComponent


