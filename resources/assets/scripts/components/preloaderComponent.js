let preloaderComponent = new function (){
  let self = this

  self.hidePreloader = () => {document.body.classList.remove('loading')}
  self.showPreloader = () => {document.body.classList.add('loading')}
};

export default preloaderComponent
