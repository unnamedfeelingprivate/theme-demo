import selectComponent from './selectComponent'
import 'whatwg-fetch'
import sha512Helper from '../util/sha512Helper'
import 'element-closest-polyfill'
import Cookies from 'js-cookie'
import searchUtils from '../util/searchUtils'
import 'abortcontroller-polyfill/dist/polyfill-patch-fetch'

let filterComponent = new function() {
  /**
   *
   * @type {filterComponent}
   */
  let self = this

  self.config = {}

  /**
   * init for this component. Will do all start heavy lifting of this object
   */
  self.init = () => {
    self.setupConfig()

    self.containerMain = document.querySelector('.inventoryArchiveComponent__container main')
    self.paginationLinks = self.containerMain.querySelectorAll('.js-ajaxPagination a')

    if(self.paginationLinks) self.paginationLinks.forEach(link => {
      link.addEventListener('click', self.handlePaginationTransition)
    })
  }

  /**
   *
   * @param endpoint
   * @param method
   * @param data
   * @param successCallback
   */
  self.fetchNewPosts = (endpoint = '', method = 'POST', successCallback = null) => {
    self.abortController = new AbortController()
    self.abortControllerSignal = self.abortController.signal;
    let errors = []

    switch(true){
      case (endpoint === ''):
        errors.push('No endpoint provided for fetchNewPosts!')
      // eslint-disable-next-line no-fallthrough
      case (self.config.query === {}):
        errors.push('No data provided for fetchNewPosts!')
    }

    if(errors.length > 0) return console.warn(errors)

    self.setActiveRequest(true)

    let fetchConf = {
      method: method,
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
      body: JSON.stringify(self.config.query), // body data type must match "Content-Type" header
      signal: self.abortControllerSignal,
    }

    self.containerMain.classList.add('loading')

    fetch(endpoint, fetchConf)
      .then(response => {
        return response.json()
      })
      .then(successCallback)
      .catch(error => console.log(error))
  }

  /**
   * Will abort all current fetch requests.
   */
  self.abortCurrentFetch = () => {
    if(self.config.activeRequest){
      self.abortController.abort()

      delete self.abortController
      delete self.abortControllerSignal

      self.setActiveRequest()

      setTimeout(() => {
        self.containerMain.classList.remove('loading')
      }, 300)
    }
  }

  /**
   * Successfull search api request callback
   * @param response
   */
  self.inventoryFetchSuccessfullCallback = response => {
    setTimeout(() => {
      self.containerMain.classList.remove('loading')
    }, 1000)

    if(self.paginationLinks) self.paginationLinks.forEach(link => {
      link.removeEventListener('click', self.handlePaginationTransition)
    })

    if (response.success && response.data.html){
      self.containerMain.querySelector('.inventoryArchiveComponent__topPanel-sortItem').classList.remove('d-none')
      self.containerMain.querySelector('.items').innerHTML = response.data.html
      self.containerMain.querySelector('.js-ajaxPagination').innerHTML = response.data.pagination

      self.paginationLinks = self.containerMain.querySelectorAll('.js-ajaxPagination a')
      if(self.paginationLinks) self.paginationLinks.forEach(link => {
        link.addEventListener('click', self.handlePaginationTransition)
      })
    } else if(!response.success && response.data.message){
      let errorHolder = document.createElement('div'),
        errorMessage = document.createElement('h1')
      errorHolder.classList.add('col-12')
      errorMessage.innerText = response.data.message
      errorHolder.appendChild(errorMessage)

      self.containerMain.querySelector('.items').innerHTML = errorHolder.outerHTML
      self.containerMain.querySelector('.js-ajaxPagination').innerHTML = null
      self.containerMain.querySelector('.inventoryArchiveComponent__topPanel-sortItem').classList.add('d-none')
    }

    self.setActiveRequest()
  }

  self.handlePaginationTransition = event => {
    event.preventDefault()
    console.log(event.target)
    console.log(self.config)
  }

  self.setupConfig = () => {
    self.config.query = {}
    let cookieConfig = Cookies.get('queryConfig') && JSON.parse(Cookies.get('queryConfig')) || {}

    if(Object.entries(cookieConfig).length > 0){
      delete cookieConfig.hash
      for (let cookieConfigKey in cookieConfig) {
        if(cookieConfig.hasOwnProperty(cookieConfigKey)){
          self.setConfigParam(cookieConfigKey, cookieConfig[cookieConfigKey], 'query')
        }
      }
    }
    self.setActiveRequest()

    let fullSearchArr = searchUtils.returnFullSearchArr()

    if(Array.isArray(fullSearchArr)){
      fullSearchArr.forEach(elem => {
        let elemArr = elem.split('=')

        if (Array.isArray(elemArr)){
          self.setConfigParam(elemArr[0], elemArr[1], 'query')
        }
      })
    }

    if(document.querySelector('[name="filterNonce"]')) self.setConfigParam('filterNonce', document.querySelector('[name="filterNonce"]').value, 'query')

    console.log(self.config)
  }

  self.setConfigParam = (param = null, value = null, targetProp = null) => {
    if(param === null) return console.warn('No param name provided for setConfigParam')

    switch(true){
      case (value === null):
        return delete (targetProp === null ? self.config[param] : self.config[targetProp][param])

      default:
        if(targetProp === null){
          return self.config[param] = value
        } else {
          return self.config[targetProp][param] = value
        }
    }
  }

  self.setActiveRequest = (status = false) => self.config.activeRequest = status
};

export default filterComponent
