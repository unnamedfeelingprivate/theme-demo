import selectComponent from './selectComponent'
import Cookie from 'js-cookie'
import filterComponent from './filterComponent'
import sha512Helper from '../util/sha512Helper'

let orderSelectComponent = new function(){
  let self = this

  self.init = () => {
    self.selects = selectComponent.getAllSelects(document.querySelector('.inventoryArchiveComponent__topPanel-sortItem'))
    self.queryConfig = Cookie.get('queryConfig') && JSON.parse(Cookie.get('queryConfig')) || {}

    if (!self.selects) return console.warn('Can not find any inventory order selects!');

    self.selects.forEach(select => {
      select.addEventListener('change', event => {
        let orderConf = event.target.value.split(':')
        self.queryConfig.orderby = orderConf[0]
        self.queryConfig.order = orderConf[1]

        Cookie.set('queryConfig', self.queryConfig, {expires: 1})

        self.queryConfig.hash = sha512Helper.b64_sha512(JSON.stringify(self.queryConfig))
        self.queryConfig.filterNonce = document.querySelector('[name="filterNonce"]').value
        delete self.queryConfig.hash

        filterComponent.fetchNewPosts('/wp-json/automotive/v1/fetchInventory', 'POST', self.queryConfig, filterComponent.inventoryFetchSuccessfullCallback)
      })
    })
  }
};

export default orderSelectComponent

