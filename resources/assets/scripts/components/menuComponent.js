import MicroModal from 'micromodal'
import modalConfig from '../util/modalConfig'
import calcViewport from '../util/calcViewport'

let menuComponent = new function(){
  let self = this

  self.scrollingElem = document.querySelector('header>.header-bottom');
  self.scrolling = false;
  self.lastScrollY = 0;
  self.scrollSetUp = false

  self.checkerFunc = (elem, customClasses = []) => {
    if(elem && customClasses.length){
      customClasses.forEach(customClass => {
        if(window.scrollY >= self.initialFromTop){
          elem.classList.add(customClass)
        } else {
          elem.classList.remove(customClass)
        }
      })
    }
  }

  self.init = () => {
    self.initialFromTop = self.scrollingElem.offsetTop
    self.viewport = calcViewport.calc()
    self.pageHeight = document.body.clientHeight

    document.querySelectorAll('.menuComponent a').forEach(link => {
      if(link.href.indexOf('#') >= 0){
        link.addEventListener('click', event => {
          event.preventDefault()
        })
      }
    })

    document.querySelectorAll('.js-searchTrigger').forEach(trigger => {
      trigger.addEventListener('click', () => {
        MicroModal.show('searchModal', modalConfig.search)
      })
    })

    window.addEventListener('resize', () => {
      self.viewport = calcViewport.calc()

      if(self.viewport.width > 768){
        if(!self.scrollSetUp) {
          window.addEventListener('scroll', self.animationFrameScrollHandler)
        }
      } else {
        self.scrollSetUp = false
        window.removeEventListener('scroll', self.animationFrameScrollHandler)
      }

    })
  }

  self.animationFrameScrollHandler = () => {
    self.scrollSetUp = true
    self.lastScrollY = window.scrollY;
    self.requestTick();
  }

  self.requestTick = () => {
    if (!self.scrolling) {
      window.requestAnimationFrame(self.update);
      self.scrolling = true;
    }
  }

  self.update = () => {
    self.checkerFunc(self.scrollingElem, ['scrolling'])
    self.scrolling = false
  }
};

export default menuComponent


