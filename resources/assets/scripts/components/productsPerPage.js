let productsPerPageComponent = new function(){
  let self = this

  self.selectors = document.querySelectorAll('.js-woocommerce--products_per_page')

  self.init = () => {
    if(self.selectors){
      jQuery(document).on('change', '.js-woocommerce--products_per_page', event => {
        let selector = event.target
        Cookies.set('products_per_page', selector.value)
        window.location.reload()
      })
    }
  }
};

export default productsPerPageComponent

