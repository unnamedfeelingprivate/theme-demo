let loanCalcucatorComponent = new function () {
  let self = this

  self.calculateBtns = document.querySelectorAll('.js-calculate_loan_payment')

  self.currency = '$'
  self.priceDel = ','
  self.currencyPos = 'left'

  self.init = () => {
    let vehicle_price,
      interest_rate,
      down_payment,
      period_month

    if(self.calculateBtns){
      self.calculateBtns.forEach(btn => {
        let calculator = btn.closest('.js-loanCalculator'),
          calculator_alert = calculator.querySelector('.calculator-alert')

        calculator.querySelectorAll('.numbersOnly').forEach(field => self.filterOnlyNumbers(field))


        btn.addEventListener('click', event => {
          self.calculate(event, calculator, calculator_alert,vehicle_price, interest_rate, down_payment, period_month)
        })
      })
    }

  }

  self.calculate = (event, calculator, calculator_alert,vehicle_price, interest_rate, down_payment, period_month) => {
    event.preventDefault();

    let validation_errors = false,
      monthly_payment = 0,
      total_interest_payment = 0,
      total_amount_to_pay = 0

    //First of all hide alert
    calculator_alert.classList.remove('visible-alert');

    //4 values for calculating
    let vals = [
      {
        type: 'vehicle_price',
        value: parseFloat(calculator.querySelector('input.vehicle_price').value),
      },
      {
        type: 'interest_rate',
        value: parseFloat(calculator.querySelector('input.interest_rate').value),
      },
      {
        type: 'period_month',
        value: parseFloat(calculator.querySelector('input.period_month').value),
      },
      {
        type: 'down_payment',
        value: parseFloat(calculator.querySelector('input.down_payment').value),
      },
      {
        type: 'down_payment',
        value: parseFloat(calculator.querySelector('input.down_payment').value),
        price: parseFloat(calculator.querySelector('input.vehicle_price').value),
      },
    ]

    // vals.forEach(val => {
    for(let i = 0; i < vals.length ; i++) {
      let validation = self.validateValue(vals[i])

      if (validation.error) {
        validation_errors = true
        self.triggerError(validation, calculator);
      }
    }

    if(!validation_errors){
      vehicle_price = vals.filter(elem => {return elem.type === 'vehicle_price'})[0].value
      interest_rate = vals.filter(elem => {return elem.type === 'interest_rate'})[0].value / 1200
      down_payment = vals.filter(elem => {return elem.type === 'down_payment'})[0].value
      period_month = vals.filter(elem => {return elem.type === 'period_month'})[0].value

      var interest_rate_unused = interest_rate  === 0 ? 1 : interest_rate;
      var mathPow = Math.pow(1 + interest_rate, period_month);

      monthly_payment = (interest_rate_unused !== 1) ?
        (vehicle_price - down_payment) * interest_rate_unused * mathPow :
        (vehicle_price - down_payment) / period_month;

      var monthly_payment_div = (mathPow - 1) === 0 ? 1 : (mathPow - 1);

      monthly_payment = monthly_payment/monthly_payment_div;
      monthly_payment = monthly_payment.toFixed(2);

      // total_amount_to_pay = parseFloat(down_payment + (monthly_payment*period_month));
      total_amount_to_pay = down_payment + (monthly_payment*period_month);

      total_amount_to_pay = interest_rate === 0 ? vehicle_price : total_amount_to_pay.toFixed(2);

      total_interest_payment = total_amount_to_pay - vehicle_price;
      total_interest_payment = (interest_rate == 0) ? 0 : total_interest_payment.toFixed(2);

      var biweelky_payments = (vehicle_price - down_payment)/(period_month/12 * 26.03555);
      var biweelky_payment = total_interest_payment/(period_month/12 * 26.03555) + biweelky_payments;
      var bipayment = biweelky_payment.toFixed(2);

      jQuery(calculator.querySelector('.stm-calculator_results')).slideDown();
      calculator.querySelector('.monthly_payment').innerText = self.formatPrice(monthly_payment);
      calculator.querySelector('.biweekly_payment').innerText = self.formatPrice(bipayment);
      calculator.querySelector('.total_interest_payment').innerText = self.formatPrice(total_interest_payment);
      calculator.querySelector('.total_amount_to_pay').innerText = self.formatPrice(total_amount_to_pay);
    } else {
      jQuery(calculator.querySelector('.stm-calculator_results')).slideUp();
      calculator.querySelector('.monthly_payment').innerText = '';
      calculator.querySelector('.total_interest_payment').innerText = '';
      calculator.querySelector('.total_amount_to_pay').innerText = '';
    }
  }

  self.filterOnlyNumbers = field => {
    let replacer = event => {
      field.value = field.value.replace(/[^0-9\.]/g,'');
      if ((event.which !== 46 || field.value.indexOf('.') !== -1) && (event.which < 48 || event.which > 57)) {
        event.preventDefault();
      }

      if ( field.value !== '' ){
        field.parentNode.classList.remove('has-error')
      }
    }

    field.addEventListener('keypress', replacer)
    field.addEventListener('keyup', replacer)
    field.addEventListener('blur', replacer)
  }

  self.validateValue = (params) => {
    let errors = true,
      msg = '',
      type = params.type || 'vehicle_price'

    switch (true) {
      case (type === 'vehicle_price' && isNaN(params.value)):
        msg = 'Please fill Vehicle Price field'
        break;

      case (type === 'interest_rate' && isNaN(params.value)):
        msg = 'Please fill Interest Rate field'
        break;

      case (type === 'period_month' && isNaN(params.value)):
        msg = 'Please fill Period field'
        break;

      case (type === 'down_payment' && isNaN(params.value)):
        msg = 'Please fill Down Payment field'
        break;

      case (type === 'down_payment' && params.value > params.price):
        msg = 'Down payment can not be more than vehicle price'
        break;

      default:
        errors = false
    }

    return {error: errors, type: type, msg: msg}
  }

  self.triggerError = (params, calculator) => {
    let calculator_alert = calculator.querySelector('.calculator-alert')
    calculator_alert.classList.add('visible-alert')

    calculator.querySelector('.'+params.type).parentNode.classList.add('has-error')
  }

  self.formatPrice = (price) => {
    // return price;

    var formattedText = '';
    if (self.currencyPos === 'left') {
      formattedText += self.currency;
      formattedText += price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, self.priceDel);
    } else {
      formattedText += price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, self.priceDel);
      formattedText += self.currency;
    }
    return formattedText;
  }
};

export default loanCalcucatorComponent
