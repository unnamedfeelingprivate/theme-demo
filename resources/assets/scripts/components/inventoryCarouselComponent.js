import Glide from '@glidejs/glide'
import calcViewport from '../util/calcViewport'
import lightGalleryComponent from './lightGalleryComponent'
import 'lightgallery.js'
import 'lightgallery.js/lib/js/lg-utils'

let singleCarouselComponent = new function(){
  let self = this

  self.sliders = document.querySelectorAll('.js-inventoryCarousel')
  self.slideConfig={
    generic: {
      type: 'carousel',
      perView: 1,
      focusAt: 'center',
      throttle: 60,
    },
    singleInventory: {
      type: 'carousel',
      perView: 1,
      focusAt: 'center',
      throttle: 60,
      swipeThreshold: false,
    },
    singleShowroom: {
      type: 'carousel',
      startAt: 0,
      perView: 1,
      focusAt: 'center',
      throttle: 60,
    },
  }
  self.slidersArr={}
  self.carouselsArr={}

  self.gotoSlide = (instance, target) => {
    // eslint-disable-next-line no-unused-vars
    return new Promise(function(resolve, reject){
      instance.go(target)
      resolve('moved')
    })
  }

  self.initInventorySliders = () => {
    if (self.sliders && self.sliders.length){
      self.sliders.forEach(sld => {
        let config = sld.dataset.confname && typeof(self.slideConfig[sld.dataset.confname]) !== 'undefined' ?
          self.slideConfig[sld.dataset.confname] : self.slideConfig.generic;

        if (sld.dataset.config && JSON.parse(sld.dataset.config) ){
          if(calcViewport.calc().width > 1024){
            Object.assign(config, JSON.parse(sld.dataset.config))
          } else {
            Object.assign(config, {perView: 1, focusAt: 0})
          }
        }

        let glideSlider = new Glide(sld, config)
        let controlledCarousel = sld.dataset.bindcontrols
        if (sld.id!==undefined) {
          self.slidersArr['#'+sld.id] = glideSlider
        }

        sld.parentNode.querySelectorAll('.js-sliderArrow').forEach(arrow => {
          arrow.addEventListener('click', () => {
            self.gotoSlide(glideSlider, arrow.dataset.glideDir).then(
              function(result){
                if (result === 'moved' && self.carouselsArr[controlledCarousel]!==undefined) {
                  self.gotoSlide(self.carouselsArr[controlledCarousel], arrow.dataset.glideDir)
                }
              }
            )
          })
        })

        glideSlider.on('run.before', function(e) {
          if (self.carouselsArr[sld.dataset.bindcontrols]!==undefined) {
            self.gotoSlide(self.carouselsArr[controlledCarousel], e.direction)
          }
        })

        glideSlider.on('mount.after', () => {
          if (sld.dataset.useslider === '1') lightGalleryComponent.initLGinInventorySlider(sld)
        })

        glideSlider.mount()
      })
    } else {
      lightGalleryComponent.initLGonElement('.js-lightGallery')
    }
  }

  self.initShowroomSliders = () => {
    self.sliders = document.querySelectorAll('.js-showroomSlider')

    if(self.sliders && self.sliders.length){
      self.sliders.forEach(sld => {
        let config = sld.dataset.confname && typeof(self.slideConfig[sld.dataset.confname]) !== 'undefined' ?
          self.slideConfig[sld.dataset.confname] : self.slideConfig.generic;

        if (sld.dataset.config && JSON.parse(sld.dataset.config) ){
          if(calcViewport.calc().width > 1024){
            Object.assign(config, JSON.parse(sld.dataset.config))
          } else {
            Object.assign(config, {perView: 1, focusAt: 0})
          }
        }

        console.log(config)

        let glideSlider = new Glide(sld, config)
        let controlledCarousel = sld.dataset.bindcontrols
        if (sld.id!==undefined) {
          self.slidersArr['#'+sld.id] = glideSlider
        }

        sld.parentNode.querySelectorAll('.js-sliderArrow').forEach(arrow => {
          arrow.addEventListener('click', () => {
            self.gotoSlide(glideSlider, arrow.dataset.glideDir).then(
              function(result){
                if (result === 'moved' && self.carouselsArr[controlledCarousel]!==undefined) {
                  self.gotoSlide(self.carouselsArr[controlledCarousel], arrow.dataset.glideDir)
                }
              }
            )
          })
        })

        glideSlider.on('run.before', function(e) {
          if (self.carouselsArr[sld.dataset.bindcontrols]!==undefined) {
            self.gotoSlide(self.carouselsArr[controlledCarousel], e.direction)
          }
        })

        glideSlider.mount()
      })
    }
  }
};

export default singleCarouselComponent


