import Choices from 'choices.js'
import searchUtils from '../util/searchUtils'
import filterComponent from './filterComponent'
import sha512Helper from '../util/sha512Helper'

let selectComponent = new function() {
  let self = this

  self.init = function () {
    self.elements = self.getAllSelects()

    if(self.elements){
      self.elements.forEach(select => {
        let config = {}

        if (select.dataset && select.dataset.initialtext) {
          config.itemSelectText = select.dataset.initialtext
        }

        new Choices(select, config)

        select.addEventListener('showDropdown', () => {
          if(filterComponent.config.activeRequest){
            filterComponent.abortCurrentFetch()
          }
        })

        if(select.dataset && select.dataset.handle === '1'){
          select.addEventListener(
            'change',
            event => self.handleSelectChange(event, select),
            false
          );
        }
      })
    }


    self.filterSelects = selectComponent.getAllSelects(document.querySelector('.filterWidgetComponent'))

    if(self.filterSelects) self.filterSelects.forEach(self.handleFilterSelectChange)
  }

  self.getAllSelects = (parent = null) => {
    parent = parent ? parent : document
    return parent.querySelectorAll('.js-select')
  }

  self.handleSelectChange = (event, select) => {
    let val = event.detail.value,
      name = select.name

    searchUtils.updateSearch(name, val)
  }



  self.handleFilterSelectChange = select => {
    select.addEventListener(
      'change',
      () => {
        let form = select.closest('form')
        //   fields = form.querySelectorAll('input,select')

        // if (fields){
        //   fields.forEach(field => {
        //     // if(field.value !== ''){
        //       // data[field.name] = field.value
        //       filterComponent.setConfigParam(field.name, field.value, 'query')
        //     // }
        //   })
        //
        //   filterComponent.setConfigParam('hash', sha512Helper.b64_sha512(JSON.stringify(filterComponent.config.query)), 'query')
        //   // data.hash = sha512Helper.b64_sha512(JSON.stringify(data))
        // }

        filterComponent.setConfigParam(select.name, select.value, 'query')
        filterComponent.setConfigParam('hash', sha512Helper.b64_sha512(JSON.stringify(filterComponent.config.query)), 'query')

        if(Object.entries(filterComponent.config.query).length > 0){
          setTimeout(() => {
            filterComponent.fetchNewPosts(form.action, 'POST', filterComponent.inventoryFetchSuccessfullCallback)
          }, 500)
        }
      },
      false)
  }
};

export default selectComponent
