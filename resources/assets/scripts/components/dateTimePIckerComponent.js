import flatpickr from 'flatpickr'

let dateTimePickerComponent = new function(){
  let self = this

  self.pickers = document.querySelectorAll('.js-dateTimePicker')

  self.init = () => {
    if(!self.pickers) return false;

    self.pickers.forEach(picker => {
      let config = {
        enableTime: false,
        // altInput: true,
        // altFormat: "F j, Y H:i",
        dateFormat: 'Y-m-d',
      }

      if(picker.dataset){
        for (let datasetKey in picker.dataset) {
          if(picker.dataset.hasOwnProperty(datasetKey)){
            config[datasetKey] = picker.dataset[datasetKey]
          }
        }
      }

      flatpickr(picker, config)
    })
  }
};

export default dateTimePickerComponent


