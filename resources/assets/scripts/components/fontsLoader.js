import WebFont from 'webfontloader/webfontloader'

let webfontComponent = new function(){
  let self = this

  self.init = () => {
    let automotiveVars = window.automotiveVars || {},
      config = {
        custom: {
          families: ['Antenna', 'automotive'],
          urls: [automotiveVars.tpld + '/assets/fonts/antenna/stylesheet.css', automotiveVars.tpld + '/assets/fonts/automotive/style.css'],
        },
      }

      if(automotiveVars.gfonts !== undefined) config.google = automotiveVars.gfonts

    WebFont.load(config);
  }
};

export default webfontComponent


