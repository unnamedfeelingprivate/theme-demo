import webfontComponent from '../components/fontsLoader'
import preloaderComponent from '../components/preloaderComponent'
import mapsComponent from '../components/googleMap'
import toTopButtonComponent from '../components/toTopButton'
import menuComponent from '../components/menuComponent'
import tabsComponent from '../components/tabsComponent'
import dateTimePickerComponent from '../components/dateTimePIckerComponent'
import urlHandlerComponent from '../components/urlHandler'
import mobileMenuComponent from '../components/mobileMenuComponent'
import selectComponent from '../components/selectComponent'

import calcViewport from '../util/calcViewport'

export default {
  init() {
    window.addEventListener('DOMContentLoaded', () => {
      webfontComponent.init()
    });

    window.addEventListener('load', () => {
      let vp = calcViewport.calc()

      mapsComponent.init()

      preloaderComponent.hidePreloader()
      toTopButtonComponent.init()

      menuComponent.init()
      if (vp.width < 768){
        mobileMenuComponent.init()
      }

      tabsComponent.init()
      dateTimePickerComponent.init()

      urlHandlerComponent.init()

      selectComponent.init()
    })

    window.addEventListener('scroll', () => {
      toTopButtonComponent.onScroll()
    })

    window.addEventListener('resize', () => {
      if (calcViewport.calc().width < 768 && mobileMenuComponent.status === false){
        mobileMenuComponent.init()
      }
    })

  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
