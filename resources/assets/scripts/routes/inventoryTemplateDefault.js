import inventoryCarouselComponent from '../components/inventoryCarouselComponent'
import loanCalcucatorComponent from '../components/loanCalculator'
import collapsibleComponent from '../components/collapsibleComponent'

export default {
  init() {
    window.addEventListener('load', () => {
      inventoryCarouselComponent.initInventorySliders()

      loanCalcucatorComponent.init()
      collapsibleComponent.init()
    })
  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};
