import filterComponent from '../components/filterComponent'
import rangeSliderComponent from '../components/rangeSliderComponent'
import orderSelectComponent from '../components/orderSelectComponent'

export default {
  init() {
    window.addEventListener('load', () => {
      filterComponent.init()
      rangeSliderComponent.init()
      orderSelectComponent.init()
    })
  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};
