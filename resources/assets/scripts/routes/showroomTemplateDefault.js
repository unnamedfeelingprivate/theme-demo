import inventoryCarouselComponent from '../components/inventoryCarouselComponent'
import resourceLazyloadComponent from '../components/resourceLazyloadComponent'

export default {
  init() {
    window.addEventListener('load', () => {
      inventoryCarouselComponent.initShowroomSliders()

      resourceLazyloadComponent.init('.js-lazyloadIframe')
    })
  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};
