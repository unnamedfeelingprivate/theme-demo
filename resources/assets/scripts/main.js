// import external dependencies
// import 'jquery';

// Import everything from autoload
import './autoload/**/*'

// import local dependencies
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import aboutUs from './routes/about';
import inventoryTemplateDefault from './routes/inventoryTemplateDefault'
import showroomTemplateDefault from './routes/showroomTemplateDefault'
import postTypeArchiveInventory from './routes/postTypeArchiveInventory'

require('custom-event-polyfill');


/** Populate Router instance with DOM routes */
const routes = new Router({
  // All pages
  common,
  // Home page
  home,
  // About Us page, note the change from about-us to aboutUs.
  aboutUs,

  inventoryTemplateDefault,
  showroomTemplateDefault,
  postTypeArchiveInventory,
});

// Load Events
document.addEventListener('DOMContentLoaded', () => {
  routes.loadEvents()
})
