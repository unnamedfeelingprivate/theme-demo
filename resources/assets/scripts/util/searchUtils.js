let searchUtils = new function(){
  let self = this

  self.init = () => {

  }

  self.updateSearch = (param = '', value = '') => {
    if(param === '') return console.warn('Wrong data provided to updateSearch method: ', {param: param, value: value, updateHistory: updateHistory})

    let search = window.location.search,
      newSearch = '?'

    if(search !== ''){
      let fullSearchArr = self.returnFullSearchArr()

      if(Array.isArray(fullSearchArr)){
        fullSearchArr.forEach(elem => {
          let elemArr = elem.split('=')

          if (Array.isArray(elemArr)){
            newSearch += (newSearch.length > 1 ? '&' : '' ) + elemArr[0] + '=' + (param === elemArr[0] ? value : elemArr[1])
          }
        })
      }
    } else {
      newSearch += param + '=' + value
    }

    if(newSearch.length > 1 && newSearch.indexOf(param) < 0) newSearch += '&' + param + '=' + value

    self.updateHistory(window.location.pathname + newSearch)
  }

  self.updateHistory = (url = '') => {
    if(url === '') return false;

    history.pushState(
      null,
      '',
      url
    )
  }

  self.getSearchItem = (name = '') => {
    let fullSearchArr = self.returnFullSearchArr()

    switch (true) {
      case (!Array.isArray(fullSearchArr) || !fullSearchArr.length):
      // case (name !== '' && typeof fullSearchArr[name] === 'undefined'):
        return false;

      case (name === '' && Array.isArray(fullSearchArr)):
        return fullSearchArr;

      default:
        let items = fullSearchArr.filter(elem => {
          return elem.indexOf(name) >= 0
        })
        return items.length && items[0].indexOf('=') && items[0].split('=')[1];
    }
  }

  self.returnFullSearchArr = () => {
    let search = window.location.search,
      fullSearchArr = search !== '' ? search.slice(1) : ''

    return fullSearchArr.split('&')
  }
};

export default searchUtils

