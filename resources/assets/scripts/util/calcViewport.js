let calcViewport= function(){
    var e = window,
      a = 'inner';
    if ( !( 'innerWidth' in window ) )
    {
      a = 'client';
      e = document.documentElement || document.body;
    }
    return { width : e[ a+'Width' ] , height : e[ a+'Height' ], used: { e: e, a: a} };
  },
  result={
    current: calcViewport(),
    calc: calcViewport
  }

module.exports=result
