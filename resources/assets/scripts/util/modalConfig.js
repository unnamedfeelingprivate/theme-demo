let modalConfig = {
  search: {
    onShow: () => {
      document.querySelector('.wrap').classList.add('modalOpened')
    },
    onClose: () => {
      document.querySelector('.wrap').classList.remove('modalOpened')
    },
    disableScroll: true,
    disableFocus: false,
    awaitOpenAnimation: true,
    awaitCloseAnimation: true,
    debugMode: true,
  },
}

export default modalConfig
